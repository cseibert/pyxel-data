{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calibration CDM beta pipeline with ``Pyxel``\n",
    "\n",
    "This notebook is an example of a 'calibration' pipeline.\n",
    "\n",
    "The purpose of the Calibration mode is to find the optimal input arguments of models or optimal detector attributes based on a target dataset the models or detector behaviour shall reproduce.\n",
    "\n",
    "The YAML configuration file looks like that:\n",
    "\n",
    "```yaml\n",
    "# YAML configuration file for Calibration mode\n",
    "simulation:\n",
    "  mode: calibration\n",
    "  \n",
    "  calibration:\n",
    "    calibration_mode:     pipeline\n",
    "    result_type:          image\n",
    "    result_fit_range:     [0, 100, 0, 50]\n",
    "    target_data_path:     ['../examples/CTI/input_data/cti/data.fits']\n",
    "    target_fit_range:     [0, 100, 0, 50]\n",
    "    seed:                 20001\n",
    "    fitness_function:\n",
    "      func:               pyxel.calibration.fitness.sum_of_abs_residuals\n",
    "      arguments:\n",
    "    algorithm:\n",
    "      type:               sade\n",
    "      generations:        15\n",
    "      population_size:    10\n",
    "      variant:            2\n",
    "    parameters:\n",
    "      - key:              pipeline.charge_transfer.cdm.arguments.beta_p\n",
    "        values:           _\n",
    "        logarithmic:      false\n",
    "        boundaries:       [0., 1.]\n",
    "        \n",
    "  outputs:                        # Define parameters for output\n",
    "    output_folder: 'output'\n",
    "    calibration_plot:             # Create a PNG image\n",
    "      champions_plot:\n",
    "      population_plot:\n",
    "    \n",
    "  ccd_detector:                   # Define detector\n",
    "    ...\n",
    "    \n",
    "  pipeline:                       # Define the pipeline\n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before starting this example, you have to install `Pyxel` in a conda environment (see the [guide to work with the code](https://esa.gitlab.io/pyxel/doc/contributing.html#working-with-the-code)).\n",
    "\n",
    "## Install Pyxel in a `conda` environment\n",
    "\n",
    "### Install Anaconda3 or Miniconda3\n",
    "\n",
    "The first step is to install [Anaconda3](https://www.anaconda.com/distribution/) or [Miniconda3](https://docs.conda.io/en/latest/miniconda.html).\n",
    "The command line tool will installed `conda`.\n",
    "\n",
    "Then open the command line and make sure that your `conda` is up to date with the following:\n",
    "\n",
    "```bash\n",
    "$ conda update conda\n",
    "...\n",
    "$ conda --version\n",
    "conda 4.8.3\n",
    "```\n",
    "\n",
    "### Get the source code\n",
    "\n",
    "Before to start, you have to get the source code with the following steps\n",
    "* Sign up at gitlab.com\n",
    "* Fork the Pyxel repo on GitLab (once) into your Gitlab account: \n",
    "  * Go to [https://gitlab.com/esa/pyxel](https://gitlab.com/esa/pyxel)\n",
    "  * Click \"Fork\" button (top right)\n",
    "* Now, the Pyxel repo is available into your Gitlab account. Clone your fork locally on your computer:\n",
    "```bash\n",
    "$ git clone https://gitlab.com/YOUR-USER-NAME/pyxel.git\n",
    "$ cd pyxel\n",
    "```\n",
    "* Configure upstream remote:\n",
    "```bash\n",
    "$ git remote add upstream https://gitlab.com/esa/pyxel.git\n",
    "```\n",
    "  * Now you can push/pull your _fork_ with ``git push`` and ``git pull``\n",
    "\n",
    "### Create a new Python environment with `conda`\n",
    "\n",
    "\n",
    "From the command line, enter the following commands:\n",
    "\n",
    "```bash\n",
    "# Go to 'pyxel' folder.\n",
    "# This folder contains the file 'environment.yml'\n",
    "$ cd pyxel\n",
    "\n",
    "# Create a new conda enviroment for 'pyxel-dev' (once)\n",
    "$ conda env create -f environment.yml\n",
    "\n",
    "# Activate the build environment\n",
    "$ conda activate pyxel-dev\n",
    "\n",
    "# Build and install Pyxel\n",
    "(pyxel-dev) $ pip install -e .\n",
    "```\n",
    "\n",
    "At this point you should be able to import `Pyxel` from\n",
    "your locally built version:\n",
    "\n",
    "```bash\n",
    "# Start Python from your 'pyxel-dev' conda environment\n",
    "(pyxel-dev) $ python\n",
    ">>> import pyxel\n",
    ">>> pyxel.__version__\n",
    "'0.5+0.gcae5a0b'\n",
    "```\n",
    "\n",
    "## Start the Jupyter notebook\n",
    "\n",
    "To run this example, you have to start a Jupyter notebook server from \n",
    "your `pyxel-dev` conda environment:\n",
    "```bash\n",
    "# Activate your conda environment\n",
    "$ conda activate pyxel-dev\n",
    "\n",
    "# Go to the examples folder\n",
    "(pyxel-dev) $ cd pyxel/examples\n",
    "(pyxel-dev) $ jupyter notebook\n",
    "...\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "import dask\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pyxel\n",
    "import seaborn as sns\n",
    "import xarray as xr\n",
    "from matplotlib import pyplot as plt\n",
    "from pyxel.pipelines import Processor"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set a scheduler for ``Dask``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a 'local' Cluster\n",
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Open the example YAML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Configuration file\n",
    "cfg = pyxel.load(\"pipeline.yaml\")\n",
    "\n",
    "cfg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating detector and detection pipeline objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "detector = cfg.ccd_detector\n",
    "pipeline = cfg.pipeline\n",
    "\n",
    "calibration = cfg.calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    calibration.num_islands,\n",
    "    calibration.num_evolutions,\n",
    "    calibration.num_best_decisions,\n",
    "    calibration.algorithm.generations,\n",
    "    calibration.algorithm.population_size,\n",
    "    calibration.topology,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Build ``Processor`` instance"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "processor = Processor(detector=detector, pipeline=pipeline)\n",
    "\n",
    "processor"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run calibration\n",
    "\n",
    "The following steps are done:\n",
    "1. Create (user-defined) islands with new random population.\n",
    "   Parallelization is achieved by the (user-defined) batch fitness evaluators.\n",
    "1. Create a new archipelago and run all islands in parallel.\n",
    "   Parallelization is achieved by the (user-defined) islands."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "ds, processors, logs, filenames = pyxel.calibration_mode(\n",
    "    calibration=calibration, detector=detector, pipeline=pipeline\n",
    ")\n",
    "\n",
    "\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.min(logs[\"best_fitness\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.relplot(\n",
    "    x=\"global_num_generations\",\n",
    "    y=\"best_fitness\",\n",
    "    kind=\"line\",\n",
    "    ci=\"sd\",\n",
    "    data=logs,\n",
    "    marker=\"o\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.relplot(\n",
    "    x=\"global_num_generations\",\n",
    "    y=\"best_fitness\",\n",
    "    hue=\"id_island\",\n",
    "    kind=\"line\",\n",
    "    data=logs,\n",
    "    marker=\"o\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Save processors and extract simulated data from them"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "all_filenames, new_ds = dask.compute(filenames, ds)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import itertools\n",
    "\n",
    "\n",
    "def flatten(list_of_lists):\n",
    "    \"Flatten one level of nesting\"\n",
    "    return itertools.chain.from_iterable(list_of_lists)\n",
    "\n",
    "\n",
    "def get_relative_filename(filename):\n",
    "    return str(Path(filename).relative_to(calibration.output_dir))\n",
    "\n",
    "\n",
    "processors_filenames = pd.DataFrame(\n",
    "    list(flatten(all_filenames)), columns=[\"filename\"]\n",
    ").applymap(get_relative_filename)\n",
    "print(f\"Processor filenames in folder: '{calibration.output_dir}'\")\n",
    "\n",
    "\n",
    "processors_filenames"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "new_ds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get simulated data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result_type = calibration.fitting.sim_output\n",
    "\n",
    "result_type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract simulated and target data\n",
    "\n",
    "from pyxel.calibration import ResultType\n",
    "\n",
    "var_name = {\n",
    "    ResultType.Image: \"simulated_image\",\n",
    "    ResultType.Signal: \"simulated_signal\",\n",
    "    ResultType.Pixel: \"simulated_pixel\",\n",
    "}[result_type]\n",
    "\n",
    "\n",
    "simulated_data = new_ds[var_name]\n",
    "target_data = new_ds[\"target\"]\n",
    "\n",
    "output_data = xr.Dataset()\n",
    "output_data[\"simulated\"] = simulated_data\n",
    "output_data[\"target\"] = target_data\n",
    "\n",
    "\n",
    "output_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting 'target'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if result_type is ResultType.Image:\n",
    "    if len(output_data[\"target\"][\"id_processor\"]) == 1:\n",
    "        # Only one processor\n",
    "        output_data[\"target\"].plot()\n",
    "    else:\n",
    "        # Several processors\n",
    "        output_data[\"target\"].plot(col=\"id_processor\")\n",
    "\n",
    "elif result_type is ResultType.Pixel:\n",
    "    output_data[\"target\"].plot.line(col=\"id_processor\", x=\"y\")\n",
    "\n",
    "else:\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting 'simulated'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if result_type is ResultType.Image:\n",
    "    output_data[\"simulated\"].plot(row=\"island\")\n",
    "\n",
    "elif result_type is ResultType.Pixel:\n",
    "    output_data[\"simulated\"].plot(row=\"island\", col=\"id_processor\")\n",
    "\n",
    "else:\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

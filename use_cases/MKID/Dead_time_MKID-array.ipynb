{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Simulating the dead time in photon-counting MKID-arrays \n",
    "\n",
    "##  Author\n",
    "January 2023&mdash;Enrico Biancalani\n",
    "\n",
    "## Keywords\n",
    "MKID detector, Imaging sensors, MKID, simulation, modelling, dead time\n",
    "\n",
    "## Summary\n",
    "### [Cf. \"Pyxel: the collaborative detection simulation framework\", Prod’homme et al., Society of Photo-Optical Instrumentation Engineers (SPIE) Conference Series 11454, 1145408 (December 2020)]\n",
    "A superconducting microwave kinetic-inductance detector (MKID) is a novel concept of photo-detector tailored for wavelengths below a few millimetres [1]. Essentially, it is an inductor-capacitor (LC) micro-resonator with a superconducting photo-sensitive area, which is capable of detecting single photons; as well as providing a measurement on their arrival time and energy. Operationally, each photon impinging on the MKID breaks a number of superconducting charge carriers, called Cooper pairs, into quasi-particles. In turn, thanks to the principle of kinetic inductance&mdash;which becomes relevant only below a superconducting critical temperature&mdash;such a strike is converted into a sequence of microwave pulses (at a few $GHz$) with a wavelength-dependent profile; which are then read out one by one, on a single channel. An important caveat is that the photo-generated pulse profiles can be distinguished only if they do not overlap in time and if the read-out bandwidth is large enough. The situation is analogous to the \"pulse pile-up\" and \"coincidence losses\" of electron-multiplier charge-coupled devices (EM-CCDs), in photon-counting mode [2]. In other words, there is a maximum limit to the achievable count rate, which is inversely proportional to the minimum distance in time between distinguishable pulse profiles: the so-called \"dead time\", which is fundamentally determined by the recombination time of quasi-particles re-forming Cooper pairs. Given this, an MKID-array per se can serve as an intrinsic integral-field spectrograph, at low resolution, without any dispersive elements or chromatic filters [3].\n",
    "\n",
    "In particular, at visible and NIR wavelengths, an MKID-based camera has the potential of revolutionising the low-resolution direct spectrography of exo-planets, thanks to the aforementioned characteristics of MKIDs, as well as their built-in high multiplexability [4]; as long as the field of view is not too bright. The example in Figure 1 shows how Pyxel was used to simulate and investigate the effect of overlapping pulse profiles for an ideal MKID-array; assuming an ideal read-out bandwidth for such a science case. With the incrementally increasing brightness level of the field of view, this temporal saturation&mdash;for photons’ arrival-time intervals smaller than the dead time&mdash;ends up affecting the contrast among the observables, thus leading to an intensity saturation. \n",
    "\n",
    "To test this saturation phenomenon, one can use Pyxel’s dynamical simulation mode, by varying the parameter relative to the input illumination. In the mosaic of images below, this corresponds to an exo-planetary system&mdash;pseudo-Fomalhaut star with injected Earth (top), resolvable only in (a), (b) and (c), and Jupiter (bottom)&mdash;observed by HabEx with a starshade, in a simulation performed with the Starshade Imaging Simulation Toolkit for Exoplanet Reconnaissance (SISTER) [5]; plus ten cosmic rays added within Pyxel, which demonstrate the ease of pipelining different models even for a brand-new type of detector. Further characteristics of photon-counting MKID-arrays to be merged into Pyxel at a later stage can be found in the MKID Exoplanet Direct-Imaging Simulator (MEDIS) simulator [6].\n",
    "\n",
    "\n",
    "![Title](data/PNGs/PyXel1.png)(a)\n",
    "![Title](data/PNGs/PyXel2.png)(b)\n",
    "![Title](data/PNGs/PyXel3.png)(c)\n",
    "![Title](data/PNGs/PyXel4.png)(d)\n",
    "![Title](data/PNGs/PyXel5.png)(e)\n",
    "![Title](data/PNGs/PyXel6.png)(f)\n",
    "\n",
    "$\\textbf{Figure 1}$&mdash;\"Mosaic of simulations showing the effect of temporal saturation for an MKID-array, which leads to an intensity saturation; by incrementally increasing the brightness level in the field of view, from (a) to (f). The effect appears when the interval between the arrival time of two photons is smaller than the dead time of the affected MKIDs in the array, assuming an ideal read-out bandwidth. The sequence of associated histograms shows how the counts ($\\#$) move towards higher intensities, until the wall of 10$^{5}$ (in arbitrary units) is reached\".\n",
    "\n",
    "### References\n",
    "\n",
    "[1] Zmuidzinas, J., \"Superconducting microresonators: physics and applications,\" Annual Review of Condensed\n",
    "Matter Physics 3, 169&ndash;214 (March 2012).\n",
    "\n",
    "[2] Wilkins, A. N., McElwain, M. W., Norton, T. J., Rauscher, B. J., Rothe, J. F., Malatesta, M., Hilton,\n",
    "G. M., Bubeck, J. R., Grady, C. A., and Lindler, D. J., \"Characterization of a photon counting EMCCD for\n",
    "space-based high contrast imaging spectroscopy of extrasolar planets,\" in [High Energy, Optical, and Infrared\n",
    "Detectors for Astronomy VI], Holland, A. D. and Beletic, J., eds., Society of Photo-Optical Instrumentation\n",
    "Engineers (SPIE) Conference Series 9154, 91540C (July 2014).\n",
    "\n",
    "[3] Mazin Lab website: http://web.physics.ucsb.edu/~bmazin; the landmark for UV-to-NIR MKID-arrays.\n",
    "\n",
    "[4] Rauscher, B. J., Canavan, E. R., Moseley, S. H., Sadleir, J. E., and Stevenson, T., \"Detectors and cool-\n",
    "ing technology for direct spectroscopic biosignature characterization,\" Journal of Astronomical Telescopes,\n",
    "Instruments, and Systems 2, 041212 (October 2016).\n",
    "\n",
    "[5] \"Starshade Imaging Simulation Toolkit for Exoplanet Reconnaissance,\" by Hildebrandt S. R., Shaklan S.\n",
    "B., Cady E. J. Turnbull M. C.: http://sister.caltech.edu.\n",
    "\n",
    "[6] Dodkins, R. H., Davis, K. K., Lewis, B., Mahashabde, S., Mazin, B. A., Lipartito, I. A., Fruitwala, N.,\n",
    "O’Brien, K., and Thatte, N., \"First Principle Simulator of a Stochastically Varying Image Plane for Photon-\n",
    "counting High Contrast Applications,\" Publications of the Astronomical Society of the Pacific 132, 104503\n",
    "(October 2020)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Modules importation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import glob\n",
    "import os\n",
    "from pathlib import Path\n",
    "\n",
    "import h5py\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.ticker as ticker\n",
    "import numpy as np\n",
    "import pyxel\n",
    "from astropy.io import fits\n",
    "from pyxel.notebook import display_html\n",
    "\n",
    "# from pyxel.run import dynamic_mode\n",
    "\n",
    "\n",
    "print(f\"Current version of Pyxel: {pyxel.__version__}.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Configuration loading"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = Path(\"Dead_time_MKID-array.yaml\").resolve()\n",
    "\n",
    "cfg = pyxel.load(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simulation’s objects creation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exposure = cfg.exposure\n",
    "detector = cfg.mkid_detector\n",
    "pipeline = cfg.pipeline\n",
    "\n",
    "pyxel.display_html(exposure)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline.readout_electronics)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data pre-processing\n",
    "### [Adapted from Matthew Kenworthy's notebooks, High Contrast Imaging, 2021]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">\n",
    "<b>N.B.:</b> in <code>SAOImageDS9</code>, coordinates start with the bottom-left-corner pixel at $(1, 1)$ but Python’s NumPy system starts from $(0, 0)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def circle_mask(im, xc, yc, rcirc):\n",
    "    \"\"\"circle_mask - take the input 2D array 'im' and evaluate the equation (x-x_c)^2 + (y-y_c)^2 < r^2,\n",
    "    for a circle of central coordinates (x_c, y_c) and of radius 'rcirc':\n",
    "    it returns a mask array with the same shape as 'im'.\"\"\"\n",
    "\n",
    "    ny, nx = im.shape\n",
    "    y, x = np.mgrid[0:nx, 0:ny]\n",
    "    r = np.sqrt((x - xc) * (x - xc) + (y - yc) * (y - yc))\n",
    "\n",
    "    return r < rcirc\n",
    "\n",
    "\n",
    "def radius_percentage(R, P):\n",
    "    \"\"\"Calculate the radius of the secondary obscuration as a percentage (P) of the aperture radius (R).\"\"\"\n",
    "\n",
    "    return P * R\n",
    "\n",
    "\n",
    "def displC(c, trim=0):\n",
    "    \"\"\"Adapted from Matthew Kenworthy's notebooks, High Contrast Imaging, 2021;\n",
    "    displC - display a Complex number c as four plots:\n",
    "    - the top two plots are (Real, Imaginary) quantities;\n",
    "    - the bottom two plots are (Amplitude, Phase);\n",
    "    - optionally, cut out the central square with a size of 'trim x trim' pixels.\"\"\"\n",
    "\n",
    "    c2 = np.copy(c)\n",
    "    if (\n",
    "        trim > 0\n",
    "    ):  # If the user specifies a trim value, cut out the centre of the image, accordingly.\n",
    "        (nx, ny) = c.shape\n",
    "        dx = int((nx - trim) * 0.5)\n",
    "        dy = int((nx - trim) * 0.5)\n",
    "        c2 = c[dx : dx + trim, dy : dy + trim]\n",
    "\n",
    "    fig = plt.figure(figsize=(10, 10))\n",
    "    axre = fig.add_subplot(221)\n",
    "    axim = fig.add_subplot(222)\n",
    "    axamp = fig.add_subplot(223)\n",
    "    axpha = fig.add_subplot(224)\n",
    "\n",
    "    im = axre.imshow(c2.real)\n",
    "    im = axim.imshow(c2.imag)\n",
    "    im = axamp.imshow(np.abs(c2))\n",
    "    im = axpha.imshow(np.angle(c2))\n",
    "\n",
    "    x_Earth = 49.2 - 1.0\n",
    "    y_Earth = 38.2 - 1.0\n",
    "    x_Jupiter = 56.8 - 1.0\n",
    "    y_Jupiter = 73.5 - 1.0\n",
    "\n",
    "    circle1 = plt.Circle((x_Earth, y_Earth), 5, color=\"g\", fill=False)\n",
    "    circle2 = plt.Circle((x_Jupiter, y_Jupiter), 10, color=\"b\", fill=False)\n",
    "    circle1a = plt.Circle((x_Earth, y_Earth), 5, color=\"g\", fill=False)\n",
    "    circle2a = plt.Circle((x_Jupiter, y_Jupiter), 10, color=\"b\", fill=False)\n",
    "\n",
    "    axre.add_artist(circle1)\n",
    "    axre.add_artist(circle2)\n",
    "    axamp.add_artist(circle1a)\n",
    "    axamp.add_artist(circle2a)\n",
    "\n",
    "    axre.text(x_Earth - 10.0, y_Earth - 7.0, \"Exo-Earth\", color=\"yellow\", fontsize=10)\n",
    "    axre.text(\n",
    "        x_Jupiter - 10.0, y_Jupiter + 15.0, \"Exo-Jupiter\", color=\"yellow\", fontsize=10\n",
    "    )\n",
    "    axamp.text(x_Earth - 10.0, y_Earth - 7.0, \"Exo-Earth\", color=\"yellow\", fontsize=10)\n",
    "    axamp.text(\n",
    "        x_Jupiter - 10.0, y_Jupiter + 15.0, \"Exo-Jupiter\", color=\"yellow\", fontsize=10\n",
    "    )\n",
    "\n",
    "    axre.set_title(\"Real part\")\n",
    "    axim.set_title(\"Imaginary part\")\n",
    "    axamp.set_title(\"Amplitude\")\n",
    "    axpha.set_title(\"Phase\")\n",
    "\n",
    "    plt.show()\n",
    "\n",
    "\n",
    "def wfits(img, file_name):\n",
    "    \"\"\"wfits - write 'img' to the file 'file_name', automatically over-writing any old file, in case.\"\"\"\n",
    "\n",
    "    hea = fits.PrimaryHDU(img)\n",
    "    hea.writeto(file_name, overwrite=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pic, pic_header = fits.getdata(\n",
    "    filename=str(pipeline.photon_collection.load_image.arguments[\"image_file\"]),\n",
    "    header=True,\n",
    ")\n",
    "pic = np.flipud(pic)\n",
    "\n",
    "displC(pic)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "?wfits"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dtel = 97\n",
    "\n",
    "y, x = np.mgrid[0:dtel, 0:dtel]\n",
    "Width = 0.01 * dtel\n",
    "\n",
    "Vane_vertical_mask_r = x < (50.0 + Width * 0.5)\n",
    "Vane_vertical_mask_l = x > (50.0 - Width * 0.5)\n",
    "Vane_vertical_mask_t = y < (50.0 + Width * 0.5)\n",
    "Vane_vertical_mask_b = y > (50.0 - Width * 0.5)\n",
    "\n",
    "S = 1.0 - circle_mask(pic, 50.0, 50.0, radius_percentage(dtel * 0.5, 0.15))\n",
    "\n",
    "Masked_pic = (\n",
    "    pic\n",
    "    * S\n",
    "    * (1.0 - Vane_vertical_mask_r * Vane_vertical_mask_l)\n",
    "    * (1.0 - Vane_vertical_mask_t * Vane_vertical_mask_b)\n",
    ")\n",
    "\n",
    "displC(Masked_pic)\n",
    "\n",
    "wfits(\n",
    "    img=np.flipud(Masked_pic),\n",
    "    file_name=\"data/sister_run_scene_9_Formalhaut_B_solar_glint_inverted_MASKED.fits\",\n",
    ")\n",
    "\n",
    "pipeline.photon_collection.load_image.arguments[\n",
    "    \"image_file\"\n",
    "] = \"data/sister_run_scene_9_Formalhaut_B_solar_glint_inverted_MASKED.fits\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dynamical simulation run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.exposure_mode(\n",
    "    exposure=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(exposure)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Output plotting\n",
    "### [This output is expected to be different from the one reported in Figure 1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path = max(glob.glob(os.path.join(\"output/\", \"*/\")), key=os.path.getmtime)\n",
    "\n",
    "Images_names = []\n",
    "\n",
    "for i in range(6):\n",
    "    Images_names.append(\n",
    "        os.path.join(path, \"detector_image_array_\" + str(i + 1) + \".fits\")\n",
    "    )\n",
    "\n",
    "Images = []\n",
    "\n",
    "for i, path in enumerate(Images_names):\n",
    "    Images.append(fits.getdata(path, ext=0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(15, 15))\n",
    "ax = []\n",
    "\n",
    "Alphabet = [\"a\", \"b\", \"c\", \"d\", \"e\", \"f\"]\n",
    "Images_ndarray = []\n",
    "\n",
    "for i in range(6):\n",
    "    ax.append(fig.add_subplot(3, 2, i + 1))\n",
    "    ax[-1].set_title(\"({})\".format(Alphabet[i]), fontsize=18)\n",
    "    Image = np.flipud(Images[i])\n",
    "    Images_ndarray.append(Image)\n",
    "    plt.imshow(Image)\n",
    "\n",
    "cmesh = plt.pcolormesh(Image, vmin=0.0, vmax=1.0e1)\n",
    "fmt = ticker.ScalarFormatter(useMathText=True)\n",
    "fmt.set_powerlimits((20.0, 0.0))\n",
    "cbar = fig.colorbar(cmesh, ax=ax, format=fmt)\n",
    "cbar.ax.yaxis.get_offset_text().set_fontsize(15)\n",
    "cbar.ax.tick_params(labelsize=10)\n",
    "cbar.set_label(\"Intensity [arbitrary unit]\", rotation=270, fontsize=15, labelpad=30)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, [(ax0, ax1), (ax2, ax3), (ax4, ax5)] = plt.subplots(3, 2, figsize=(15, 20))\n",
    "ax = [ax0, ax1, ax2, ax3, ax4, ax5]\n",
    "\n",
    "for i in range(6):\n",
    "    Image = np.flipud(Images_ndarray[i])\n",
    "\n",
    "    ax[i].hist(Image.flatten(), bins=range(0, 100 + 5, 5), align=\"mid\")\n",
    "    ax[i].tick_params(labelsize=10)\n",
    "    ax[i].set_title(\"Histogram of ({})\".format(Alphabet[i]), fontsize=18)\n",
    "    ax[i].grid(True, alpha=0.5)\n",
    "    ax[i].set_xlabel(\"Intensity [arbitrary unit]\", fontsize=15)\n",
    "    ax[i].set_ylabel(\"#\", fontsize=15)\n",
    "    ax[i].set_xlim(left=0)\n",
    "    ax[i].set_ylim(top=1.0e4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Yet-to-implement:\n",
    "\n",
    "### Set-up / tweak of the physical parametres underlying the simulation \n",
    "#### [Configuration not yet physically consistent]\n",
    "\n",
    "\n",
    "![Title](data/PNGs/Dead_time_background.png)\n",
    "\n",
    "$\\textbf{Figure 2}$&mdash;Here, the superconducting gap’s energy $\\Delta \\approx 1.76 \\ k_{B} T_{c}$, where $k_{B}$ is the Boltzmann's constant and $T_{c}$ is the superconducting critical temperature of the photo-sensitive inductor, within a single MKID."
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": true
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

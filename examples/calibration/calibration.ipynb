{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Calibration CDM irradiated image pipeline with ``Pyxel``\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "\n",
    "[The Pyxel development team](mailto:pyxel@esa.int)\n",
    "\n",
    "## Keywords\n",
    "\n",
    "Calibration\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`calibration_mode`  | Necessary | Background |\n",
    "| {ref}`calibration_mode_visualization` | Helpful | |\n",
    "| {ref}`configuration` | Helpful | | \n",
    "| [Dask/Distributed](https://distributed.dask.org/en/stable/quickstart.html) | Helpful | |\n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "\n",
    "## Learning Goals\n",
    "\n",
    "* What is the Calibration mode\n",
    "* How to run the Calibration mode\n",
    "\n",
    "## Summary\n",
    "\n",
    "This notebook is an example of a 'calibration' pipeline.\n",
    "\n",
    "The purpose of the Calibration mode is to find the optimal input arguments of models or optimal detector attributes based on a target dataset the models or detector behaviour shall reproduce. It is based on using the library PyGMO ([https://esa.github.io/pygmo/](https://esa.github.io/pygmo/)).\n",
    "\n",
    "If you haven't installed Pyxel yet, visit [https://esa.gitlab.io/pyxel/doc/tutorials/install.html](https://esa.gitlab.io/pyxel/doc/tutorials/install.html) for instructions.\n",
    "\n",
    "The YAML configuration file looks like that:\n",
    "\n",
    "```yaml\n",
    "# YAML configuration file for Calibration mode\n",
    "\n",
    "calibration:\n",
    "  \n",
    "    mode:                 pipeline\n",
    "    result_type:          image\n",
    "    result_fit_range:     [0, 100, 0, 50]\n",
    "    target_data_path:     ['../examples/CTI/input_data/cti/data.fits'] #Specifiy target data\n",
    "    target_fit_range:     [0, 100, 0, 50]\n",
    "    seed:                 20001\n",
    "    fitness_function:\n",
    "      func:               pyxel.calibration.fitness.sum_of_abs_residuals\n",
    "      arguments:\n",
    "    algorithm:\n",
    "      type:               sade\n",
    "      generations:        15\n",
    "      population_size:    10\n",
    "      variant:            2\n",
    "    parameters:\n",
    "      - key:              pipeline.charge_transfer.cdm.arguments.beta_p\n",
    "        values:           _\n",
    "        logarithmic:      false\n",
    "        boundaries:       [0., 1.]\n",
    "        \n",
    "  outputs:                        # Define parameters for output\n",
    "    output_folder: 'output'\n",
    "    calibration_plot:             # Create a PNG image\n",
    "      champions_plot:\n",
    "      population_plot:\n",
    "    \n",
    "  ccd_detector:                   # Define detector\n",
    "    ...\n",
    "    \n",
    "  pipeline:                       # Define the pipeline\n",
    "    ...\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pathlib import Path\n",
    "\n",
    "import dask\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pyxel\n",
    "import seaborn as sns\n",
    "import xarray as xr\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Set a scheduler for ``Dask``"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a 'local' Cluster\n",
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Open the example YAML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Configuration file\n",
    "config = pyxel.load(\"calibration.yaml\")\n",
    "\n",
    "config"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display the configuration\n",
    "\n",
    "### Simulation\n",
    "Simulation stores the mode, the `Outputs` class and information about the advanced usage modes: parametric, calibration and dynamic. We can display the configuration dictionary separately using `jupyxel.py` from utilities."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "pyxel.display_html(config)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pipeline"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "pyxel.display_html(pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display a model"
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "pyxel.display_html(pipeline.charge_transfer.cdm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating calibration, detector and detection pipeline objects\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "detector = config.detector\n",
    "pipeline = config.pipeline\n",
    "calibration = config.calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(\n",
    "    calibration.num_islands,\n",
    "    calibration.num_evolutions,\n",
    "    calibration.algorithm.generations,\n",
    "    calibration.algorithm.population_size,\n",
    "    calibration.topology,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run calibration\n",
    "\n",
    "The following steps are done:\n",
    "1. Create (user-defined) islands with new random population.\n",
    "   Parallelization is achieved by the (user-defined) batch fitness evaluators.\n",
    "1. Create a new archipelago and run all islands in parallel.\n",
    "   Parallelization is achieved by the (user-defined) islands."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "ds, processors, logs, filenames = pyxel.calibration_mode(\n",
    "    calibration=calibration,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Display logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ds"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.min(logs[\"best_fitness\"])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.relplot(\n",
    "    x=\"global_num_generations\",\n",
    "    y=\"best_fitness\",\n",
    "    kind=\"line\",\n",
    "    ci=\"sd\",\n",
    "    data=logs,\n",
    "    marker=\"o\",\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.relplot(\n",
    "    x=\"global_num_generations\",\n",
    "    y=\"best_fitness\",\n",
    "    hue=\"id_island\",\n",
    "    kind=\"line\",\n",
    "    data=logs,\n",
    "    marker=\"o\",\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Get simulated data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "result_type = calibration.result_type\n",
    "\n",
    "result_type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Extract simulated and target data\n",
    "\n",
    "var_name = {\n",
    "    \"image\": \"simulated_image\",\n",
    "    \"signal\": \"simulated_signal\",\n",
    "    \"pixel\": \"simulated_pixel\",\n",
    "}[result_type]\n",
    "\n",
    "simulated_data = new_ds[var_name]\n",
    "target_data = new_ds[\"target\"]\n",
    "\n",
    "output_data = xr.Dataset()\n",
    "output_data[\"simulated\"] = simulated_data\n",
    "output_data[\"target\"] = target_data\n",
    "\n",
    "\n",
    "output_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting 'target'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if result_type == \"image\":\n",
    "    if len(output_data[\"target\"][\"id_processor\"]) == 1:\n",
    "        # Only one processor\n",
    "        output_data[\"target\"].plot()\n",
    "    else:\n",
    "        # Several processors\n",
    "        output_data[\"target\"].plot(col=\"id_processor\")\n",
    "\n",
    "elif result_type == \"pixel\":\n",
    "    output_data[\"target\"].plot.line(col=\"id_processor\", x=\"y\")\n",
    "\n",
    "else:\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting 'simulated'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if result_type == \"image\":\n",
    "    output_data[\"simulated\"].plot(row=\"island\")\n",
    "\n",
    "elif result_type == \"pixel\":\n",
    "    output_data[\"simulated\"].plot(row=\"island\", col=\"id_processor\")\n",
    "\n",
    "else:\n",
    "    raise NotImplementedError"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

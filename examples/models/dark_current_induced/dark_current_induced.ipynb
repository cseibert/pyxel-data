{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ee22d274-83fe-49a4-aeef-e7ed86870f72",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Simulating radiation induced dark current\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Florian Moriousef (ISAE-SUPAERO)\n",
    "\n",
    "## Keywords\n",
    "radiation induced dark-current, displacement damage effect, CMOS, radiation\n",
    "\n",
    "## Summary\n",
    "This model \"radiation_induced_dark_current\" inside the charge generation model group adds dark current induced by radiation for Silicon CMOS detectors. The input parameters of the model are the ```depletion_volume``` parameter in µm3, ```annealing_time``` in weeks and the ```displacement_dose``` in TeV/g. Shot noise can be included when setting the paramter ```shot_noise``` to True.\n",
    "\n",
    "![Title](images/displacement_damage.png)\n",
    "$\\textbf{Figure 1}$ Definition of mean dark current increase induced by displacement damage.\n",
    "\n",
    "\n",
    "![Title](images/damage_induced.png)\n",
    "$\\textbf{Figure 2}$ Definition of the displacement damage induced dark current distribution.\n",
    "\n",
    "\n",
    "![Title](images/measurements.png)\n",
    "$\\textbf{Figure 3}$ Validation of model with measuements.\n",
    "\n",
    "A more detailed description of the models can be found in [1] and [2].\n",
    "\n",
    "### References\n",
    "[1]\n",
    "Alexandre Le Roch, Cédric Virmontois, Philippe Paillet, Jean-Marc Belloir, Serena Rizzolo, Federico Pace, Clémentine Durnez, Pierre Magnan, and Vincent Goiffon. Radiation-induced leakage current and electric field enhancement in cmos image sensor sense node floating diffusions. IEEE Transactions on Nuclear Science, 66(3):616–624, 2019. doi:10.1109/TNS.2019.2892645.\n",
    "\n",
    "[2]\n",
    "Jean-Marc Belloir, Vincent Goiffon, Cédric Virmontois, Mélanie Raine, Philippe Paillet, Olivier Duhamel, Marc Gaillardin, Romain Molina, Pierre Magnan, and Olivier Gilard. Pixel pitch and particle energy influence on the dark current distribution of neutron irradiated cmos image sensors. Opt. Express, 24(4):4299–4315, Feb 2016. URL: https://opg.optica.org/oe/abstract.cfm?URI=oe-24-4-4299, doi:10.1364/OE.24.004299."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76725163-289e-4750-a629-fc296b5270bf",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel\n",
    "from pyxel import display_detector\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "pyxel.__version__"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a1ab07d3-a079-474f-82ec-3d982c1d0e63",
   "metadata": {},
   "source": [
    "### Exposure mode\n",
    "\n",
    "See induced dark current with ```display_detector``` on pixel array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40d6ac6d-bb7b-4ac2-91b6-5df9152a6f3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"dark_current_induced.yaml\")\n",
    "\n",
    "exposure = config.exposure\n",
    "pipeline = config.pipeline\n",
    "detector = config.detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c93b11df-d4b4-494c-bacb-a3b422db8823",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree = pyxel.run_mode(mode=exposure, pipeline=pipeline, detector=detector)\n",
    "\n",
    "data_tree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6dc335c1-ccbf-41a6-9580-d0c5825d2836",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_detector(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9c19dfce-8599-4cde-93c5-741ac9eb13be",
   "metadata": {},
   "source": [
    "### Observation mode\n",
    "\n",
    "See induced dark current versus temperature for different annealing times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2db9b636-9480-4f5e-949c-e62a54594610",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"dark_current_induced_observation.yaml\")\n",
    "\n",
    "observation = config.observation\n",
    "pipeline = config.pipeline\n",
    "detector = config.detector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c2dfc9d5-eb99-4b99-a280-4c847090fe0a",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.observation_mode(\n",
    "    observation=observation, pipeline=pipeline, detector=detector\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "958d18a7-7ee4-4fb0-bad9-78c207ac30e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current = result.dataset.pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    "temperature = result.dataset.temperature.to_numpy()\n",
    "# dark current with pattern noise and figure of merit=0.01\n",
    "dark_current_induced = (\n",
    "    result.dataset.sel(annealing_time=0.1).pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8416dbf8-347a-4666-ad93-1c92b6503c9b",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.dataset.annealing_time)):\n",
    "    plt.plot(\n",
    "        temperature,\n",
    "        dark_current[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"{result.dataset.annealing_time.values[i]}\" r\" $\\mathrm{week}$\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature for different annealing times\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a931177-7f0a-484e-ad67-7835650d040f",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

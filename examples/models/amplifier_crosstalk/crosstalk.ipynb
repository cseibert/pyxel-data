{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Amplifier crosstalk model\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../../../images/pyxel_logo.png\" width=\"250\">\n",
    "\n",
    "## Authors\n",
    "\n",
    "Matej Arko\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`exposure_mode`  | Necessary | Background |\n",
    "| [YAML](https://the-turing-way.netlify.app/reproducible-research/renv/renv-yaml.html?highlight=yaml) | Helpful | Background |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "\n",
    "This notebook is an example of using the amplifier crosstalk model. For installation instructions and basic use of `Pyxel` refer to the basic example."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How it works\n",
    "\n",
    "Amplifier crosstalk develops when two or more channels are reading the detector simultaneously, producing ghost images between channels. These can be either a fraction of the image from a different channel (**DC mode**), or a fraction of the derivative of the signal from the other channel, in the order the pixels are read (**AC mode**). Coupling between different channels is described by a square **coupling matrix** $a_{k,j}$. \n",
    "\n",
    "For DC mode and a two-channel detector the matrix could be for example:\n",
    "\n",
    "$$\n",
    "\\begin{bmatrix}\n",
    "1 & 0.01 \\\\\n",
    "0.01 & 1 \n",
    "\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "which would mean that $1\\%$ of the signal incident on output $1$ is seen in output $2$, and $1\\%$ of the signal incident on output $2$ is seen in output $1$. In general, the coupling matrix is not necessarily symmetric. We could write the DC crosstalk signal for the as $X_{k,j,i}=a_{k,j}S_{k,j,i}$, where $k$ and $j$ are indices over the amplifiers and $i$ is the pixel index.\n",
    "\n",
    "In AC mode, the coupling matrix looks similar, but the extra signal you add in each output would be $X_{k,j,i}=a_{k,j}(S_{k,j,i}-S_{k,j,i-1})$. Paper that provides a full explanation with plots can be found here: https://arxiv.org/abs/1808.00790.\n",
    "\n",
    "An example of YAML code for the model in the readout electronics stage would look like this:\n",
    "```yaml\n",
    "- name: dc_crosstalk  # or ac_crosstalk\n",
    "  func: pyxel.models.readout_electronics.dc_crosstalk\n",
    "  enabled: true\n",
    "  arguments:\n",
    "    coupling_matrix: [[1,0.01],[0.01,1]]  # list or str for a file upload\n",
    "    channel_matrix: [1,2]  # list\n",
    "    readout_directions: [1,2]  # list\n",
    "```\n",
    "\n",
    "Beside the coupling matrix, which can be alternatively also a file upload, the model also needs two extra arguments. First is **channel geometry**, which is also described by a 1D or 2D matrix: `[1,2,3,4]` would mean that channels $1-4$ are arranged in four columns, `[[4,3],[1,2]]` would mean that detector is split into four regions symmetricaly, where channel $1$ is bottom left, channel $4$ upper left etc.\n",
    "\n",
    "Assuming rows are always the fast readout direction, we also need to specify the staring point of the **readout directions** for different channels. Let's say we have a four channel detector, where channel $1$ starts readout in the bottom left (numbered as 3), $2$ bottom right (numbered as 4), channel $3$ upper right (numbered as 2) and channel $4$ upper left (numbered as 1). The matrix for such configuration would be `[3,4,2,1]`. \n",
    "\n",
    "Let's see an example of the two types of crosstalk on an image of Saturn. All the matrices in the examples are non-realistic."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pyxel\n",
    "\n",
    "# For demonstration, we will also use two of the core functions from the model:\n",
    "from pyxel.models.charge_measurement.amplifier_crosstalk import (\n",
    "    crosstalk_signal_ac,\n",
    "    crosstalk_signal_dc,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "image = pyxel.load_image(\"data/saturn.fits\").astype(dtype=float)\n",
    "plt.imshow(image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DC mode\n",
    "\n",
    "Let's imagine a detector with two columnar channels, read out in opposite directions $1$ and $2$, where $20\\%$ of the image from the first channel is seen on the second one. Since the readout is in opposite directions, we should see a symmetric ghost image of saturn on the right half of the detector."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array = image.copy()\n",
    "coupling_matrix = np.array([[1.0, 0.1], [0.0, 1.0]])\n",
    "channel_matrix = np.array([1, 2])\n",
    "readout_directions = np.array([1, 2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dc_crosstalk_image = crosstalk_signal_dc(\n",
    "    array=array,\n",
    "    coupling_matrix=coupling_matrix,\n",
    "    channel_matrix=channel_matrix,\n",
    "    readout_directions=readout_directions,\n",
    ")\n",
    "plt.clf()\n",
    "plt.imshow(dc_crosstalk_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### AC mode\n",
    "\n",
    "Now, let's look at a a four channel detector with the data below. We expect to see a ghost image channels $2$, $3$ and $4$, where the ghost image is now a derivative of the original image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "array = image.copy()\n",
    "coupling_matrix = np.array(\n",
    "    [\n",
    "        [1.0, 0.5, 0.5, 0.5],\n",
    "        [0.0, 1.0, 0.0, 0.0],\n",
    "        [0.0, 0.0, 1.0, 0.0],\n",
    "        [0.0, 0.0, 0.0, 1.0],\n",
    "    ]\n",
    ")\n",
    "channel_matrix = np.array([[1, 2], [3, 4]])\n",
    "readout_directions = np.array([1, 2, 3, 4])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ac_crosstalk_image = crosstalk_signal_ac(\n",
    "    array=array,\n",
    "    coupling_matrix=coupling_matrix,\n",
    "    channel_matrix=channel_matrix,\n",
    "    readout_directions=readout_directions,\n",
    ")\n",
    "plt.clf()\n",
    "plt.imshow(ac_crosstalk_image)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Usage with `Pyxel`\n",
    "\n",
    "We can now use the model with Pyxel on an image of the Horsehead nebula by uploading the input YAML configuration file. This time the detector the detector will have 4 columnar channels, with readouts in alternating directions `[1,2,1,2]`. We will use AC mode and assume that channels $1$ and $2$ are coupled together as well as channels $3$ and $4$, symmetricaly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Open the example YAML file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"crosstalk.yaml\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Creating detector and detection pipeline objects\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Displaying the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_html(pipeline.charge_measurement.ac_crosstalk)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Processing the detector through the pipeline\n",
    "\n",
    "Now that the configuration is loaded, we have a detection pipeline object and a detector object. To start the exposure mode pipeline, we can run it by calling the function `exposure_mode`, passing the detector, pipeline and the instance of `Exposure` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.run_mode(mode=exposure, detector=detector, pipeline=pipeline)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Display final image on detector\n",
    "\n",
    "Among others, a prominent artifact in the shape of a dark stripe can be seen in the Horsehead as a result of AC crosstalk."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Extract Region of Interest(s)\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Bradley Kelman\n",
    "\n",
    "## Keywords\n",
    "source extractor, data processing\n",
    "\n",
    "## Learning Goals\n",
    "* extract sources from background in image\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`exposure_mode`  | Necessary | Background |\n",
    "| [Xarray DataTree](https://xarray-datatree.readthedocs.io/en/latest/quick-overview.html) | Helpful | |\n",
    "| [Xarray](https://xarray.dev) | Helpful | |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "This notebook shows the usage of the model ```extract_roi_to_xarray```, which extracts the source data of the final pixel array and output in the form of an xarray dataset. The models makes use of the [SEP library](https://sep.readthedocs.io/en/v1.1.x/index.html) which has taken the original source extractor package and configured it into a library of stand-alone functions and classes.\n",
    "\n",
    "The SEP library is a useful post-processing tool capable of calculating statistics of a given array."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running Pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import pyxel\n",
    "import pyxel.models.data_processing.source_extractor as pysep\n",
    "import sep\n",
    "import xarray as xr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exposure simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "config = pyxel.load(\"yaml_files/SEP_exposure.yaml\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "data_tree = pyxel.run_mode(\n",
    "    mode=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "with xr.set_options(display_style=\"text\"):\n",
    "    print(data_tree)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result = data_tree.to_dataset()\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Saving the 2d pixel result ready to be used later by the SEP function within Pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "image_2d = np.array(result[\"pixel\"].isel(time=0))\n",
    "\n",
    "image_2d"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the `extract_roi_to_xarray` is used in the yaml file, the roi data can be found under detector.data\n",
    "\n",
    "In the image there are lots of small dots that represent galaxy images. Say we wanted to extract information from them like their brightness and position within the 2d image this can be done by using the `extract_roi_to_xarray` model. Under the 'data variables' tab in the output of the next cell shows all of the information that was obtained for each roi. More details for what each varibale represents can be found at the SEP website: https://sep.readthedocs.io/en/v1.1.x/api/sep.extract.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "roi = detector.data[\"source_extractor\"]\n",
    "\n",
    "roi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plots and processing "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Inital plot of final detector image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This plot shows the final detector image after simulating an exposure with the CTI induced detector effect."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "m, s = np.mean(image_2d), np.std(image_2d)\n",
    "pysep.show_detector(image_2d, m - s, m + s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plotting the regions of interest over final detector image"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pyxel can now overlay ellipses over the resultant simulated image using the plot_roi function below. Each red ellipse represents a roi found by the extrac roi function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "roi_plot = pysep.plot_roi(data=image_2d, roi=roi)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Other SEP functionality within Pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If an exposure has been simulated and the `extract_roi_to_xarray` model has been mistakenly not used, instead of rerunning the simulations with the model it is possible to use sep.extract direcctly to obtain the same roi information. The only difference is that the output will be in the form of a structure numpy array. This does not affect the ability of the plot_roi function however as can be seen, either a numpy array or xarray is complatible with this function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "objects, segmap = sep.extract(image_2d, thresh=80, segmentation_map=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "roi_plot = pysep.plot_roi(data=image_2d, roi=objects)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "SEP is also capable of obtaining the background of the image, getting information about it and substracting this background from the original image. Here you can see the the subtract_background function outputs a background subtracted array that can then be represnted as an image. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "background_subtracted_array = pysep.subtract_background(image_2d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "m, s = np.mean(image_2d), np.std(image_2d)\n",
    "pysep.show_detector(background_subtracted_array, vmin=0, vmax=50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is also possible to obtain other information about the background such as the background rms array, the global mean rms of an image and the global mean background signal."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "background_data = pysep.get_background_data(image_2d)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "background_data[0]  # rms data array"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "background_data[1]  # global mean rms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "background_data[2]  # global mean background signal level"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

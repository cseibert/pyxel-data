# ###########################################################  #
# Pyxel detector simulation framework                          #
#                                                              #
# YAML configuration file for a CCD                            #     
# Author: Constanze Seibert 01-06-2023                         #
# ###########################################################  #

# yaml-language-server: $schema=https://esa.gitlab.io/pyxel/doc/latest/pyxel_schema.json
observation:

  parameters:    
    - key: pipeline.charge_generation.simple_dark_current.arguments.dark_rate
      values: numpy.arange(0.0, 10000.0, 1000.0)
    
  readout:
    times: [2.000e-02, 6.000e-02, 1.200e-01, 2.000e-01, 3.000e-01, 5.000e-01,
       8.000e-01, 1.200e+00, 1.700e+00, 2.300e+00, 3.000e+00, 3.800e+00,
       4.700e+00, 5.700e+00, 6.800e+00, 8.000e+00, 9.300e+00, 1.070e+01,
       1.220e+01, 1.380e+01, 1.550e+01, 1.730e+01, 1.920e+01, 2.120e+01,
       2.330e+01, 2.550e+01, 2.780e+01, 3.020e+01, 3.270e+01, 3.530e+01,
       3.800e+01, 4.080e+01, 4.370e+01, 4.670e+01, 4.980e+01, 5.300e+01,
       5.630e+01, 5.970e+01, 6.320e+01, 6.680e+01, 7.050e+01, 7.430e+01,
       7.820e+01, 8.220e+01, 8.630e+01, 9.050e+01, 9.480e+01, 9.920e+01,
       1.037e+02, 1.083e+02, 1.130e+02, 1.178e+02, 1.227e+02, 1.277e+02]
    non_destructive:  true
    
  outputs:
    output_folder: "output"
    save_data_to_file:
      
ccd_detector:

  geometry:

    row: 100 # pixel            
    col: 100 # pixel
    total_thickness: 40.    # um  
    pixel_vert_size: 12.    # um
    pixel_horz_size: 12.    # um

  environment:
    temperature: 153  

  characteristics:
    quantum_efficiency: 1         
    charge_to_volt_conversion: 7.6e-6 #V/e-  
    pre_amplification: 1.623 #V/V
    adc_voltage_range: [0.,4.] #V 
    adc_bit_resolution: 16
    full_well_capacity: 175000  #e- 

pipeline:
  # -> photon
  photon_collection:
  
    - name: load_image
      func: pyxel.models.photon_collection.load_image
      enabled: true
      arguments:
        image_file: https://gitlab.com/esa/pyxel-data/-/raw/master/examples/exposure/data/Pleiades_HST.fits
        align: "center"
  
    - name: shot_noise
      func: pyxel.models.photon_collection.shot_noise
      enabled: true

  # photon -> charge
  charge_generation:
    - name: photoelectrons
      func: pyxel.models.charge_generation.simple_conversion
      enabled: true
      
    - name: simple_dark_current
      func: pyxel.models.charge_generation.simple_dark_current
      enabled: true
      arguments:
        dark_rate: 20


  # charge -> pixel
  charge_collection:
    - name: simple_collection
      func: pyxel.models.charge_collection.simple_collection
      enabled: true
      
    - name: fixed_pattern_noise
      func: pyxel.models.charge_collection.fixed_pattern_noise
      enabled: false
      arguments:
        fixed_pattern_noise_factor: 0.014
        seed: 12345
        
  # pixel -> pixel
  charge_transfer:

  # pixel -> signal
  charge_measurement:
  
    - name: simple_measurement
      func: pyxel.models.charge_measurement.simple_measurement
      enabled: true
        
    - name: output_noise
      func: pyxel.models.charge_measurement.output_node_noise
      enabled: true
      arguments:
        std_deviation: 5.4585266113281245e-05
            
  # signal -> image
  readout_electronics:
      
    - name: simple_amplifier
      func: pyxel.models.readout_electronics.simple_amplifier
      enabled: true
        
    - name: simple_adc
      func: pyxel.models.readout_electronics.simple_adc
      enabled: true
      
      
  # -> data
  data_processing:
  
    - name: mean_variance
      func: pyxel.models.data_processing.mean_variance
      enabled: true
      arguments:
        data_structure: image #default value is image
        
    - name: stat
      func: pyxel.models.data_processing.statistics
      enabled: true
      arguments:
        data_structure: all #default value is all (“pixel”, “photon”, “signal, “image”)

    - name: linear_regression
      func: pyxel.models.data_processing.linear_regression
      enabled: true
      arguments:
        data_structure: image #default value is image
    
    - name: snr
      func: pyxel.models.data_processing.signal_to_noise_ratio
      enabled: true
      arguments:
        data_structure: signal


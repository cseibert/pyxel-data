{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f146ed54-e6e5-4108-9240-7c34efc1342a",
   "metadata": {},
   "source": [
    "# [Data processing models:](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) Exposure mode\n",
    "\n",
    "##  Author\n",
    "2023&mdash;Constanze Seibert\n",
    "\n",
    "## Keywords\n",
    "data processing, flat field simulation, exposure mode\n",
    "\n",
    "## Learning Goals\n",
    "* use of [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/)\n",
    "* use of [Data processing models](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/data_processing_models.html) in exposure mode\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`exposure_mode`  | Necessary | Background |\n",
    "| [Xarray DataTree](https://xarray-datatree.readthedocs.io/en/latest/quick-overview.html) | Helpful | |\n",
    "| [Xarray](https://xarray.dev) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "In the development towards Pyxel version 2.0, we introduced two new model groups. The **Data processing** model group at the end of the pipeline aims to do some Data processing inside of Pyxel. The image below shows the planned architecture for version 2.0.\n",
    "\n",
    "<img style=\"float: right;\" src=\"images/pipeline.png\" width=\"350\">\n",
    "\n",
    "This notebook shows the usage of several basic operation models in the model group **Data processing**, namely ```statistics```, ```mean_variance```, ```linear_regression``` and ```signal_to_noise_ratio``` in [exposure mode](https://esa.gitlab.io/pyxel/doc/latest/background/running_modes/exposure_mode.html). \n",
    "Another model, which is available since version 1.8 is the Source extractor model ```extract_roi_to_xarray```, where a seperate notebook is available [here](https://esa.gitlab.io/pyxel-data/examples/models/data_processing/source_extractor/SEP_exposure.html).\n",
    "\n",
    "In this example we are simulating flat field images with increasing integration times."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c555ad9-9222-4b09-b65d-29dfc2288415",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import packages\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import xarray as xr\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c55bc9e4-9081-40b2-88c9-9b9fad42df24",
   "metadata": {},
   "outputs": [],
   "source": [
    "# load configuration file\n",
    "config = pyxel.load(\"data_example.yaml\")\n",
    "\n",
    "exposure = config.exposure\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14cf6f7b-a952-4467-82d5-5d4138d0cbea",
   "metadata": {},
   "source": [
    "Since version 1.8, Pyxel function ```run_mode()``` returns a [xarray.DataTree](https://xarray-datatree.readthedocs.io/en/latest/) and contains the output of the (Processed) Data from each model used in the YAML configuration file. \n",
    "Datatree is a prototype implementation of a tree-like hierarchical data structure for xarray.\n",
    "\n",
    "\n",
    "### Result retrieved with ```run_mode()``` will show DataTree structure\n",
    "**Data group** contains the groups for each Data processing model used in the YAML configuration file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d06237cf-997c-484d-b2ec-8dd916f4dc7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bca09f3e-6453-487a-97aa-60efb153a6ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.set_options(display_style=\"html\")  # text or html\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c0992f2-5447-4bd1-b10b-f024b4cab9a8",
   "metadata": {},
   "source": [
    "The function ```run_mode()``` is still under development and may change again in future. With the functions ```exposure_mode()```, ```observation_mode()``` and ```calibration_mode()``` it is also possible to access the **(Processed) Data** using ```detector.data```."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2a5acc43-3e36-4c10-8fb6-2be04ee63521",
   "metadata": {},
   "outputs": [],
   "source": [
    "result1 = pyxel.exposure_mode(\n",
    "    exposure=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18bb9d68-54dd-4e4e-a3d4-9c72bef2727e",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9db11242-6349-437e-8de6-8807165d49e5",
   "metadata": {},
   "source": [
    "Currently, we get the same output using the result retrieved from the ```run_mode()``` and the ```detector.data```. In a future Pyxel version the old usage will maybe deprecated.\n",
    "\n",
    "### Statistics model example\n",
    "\n",
    "Another way to access the **(Processed) Data** of a Data processing model is with \n",
    "```detector.data.<model_name>```, e.g. ```detector.data.statistics```. \n",
    "The model ```statistics``` can be used to do simple statistics computations,\n",
    "giving the  **variance, mean, min, max** and **count** of the data buckets\n",
    "*photon, pixel, signal* and *image* of the detector along the time.\n",
    "The calculated statistics can then be accessed via ```detector.data.statistics```.\n",
    "We can also access each bucket and calculation like a path of the DataTree."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84db3038-0f21-4c84-990a-4b133f714aa4",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/statistics/pixel/var\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48d2d367-b7da-4fb5-a086-dd1a868926a0",
   "metadata": {},
   "source": [
    "The same is possible with the dot annotation, but **be careful** since *.var* is a function itself, so it will calculate again the variance and not give just the output of the calculated variance!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e9ff875e-a848-4ecc-9f7d-b88695c4d0d0",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.data.statistics.pixel.var"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53250b86-0c59-40c4-8195-ec4e80578076",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/statistics/pixel/mean\"].plot()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "a9f9a0a3-9744-4ff4-a573-9803c459d90d",
   "metadata": {},
   "source": [
    "Beware, the readout-time does not necessarily corresponds to the exposure or integration duration depending on the selected readout mode.\n",
    "\n",
    "With ```non_destructive: true``` we are having **non-destructive** readout mode. While reading pyxel keeps integrating the light.\n",
    "\n",
    "With ```non_destructive: false``` we are having **destructive** readout mode. (Typical usage for monolytic CCDs)\n",
    "\n",
    "The integration duration corresponds to $readouttime_\\left(i+1\\right)-readouttime_\\left(i\\right)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38e37ab3-9d80-4644-a2b5-1ff7f994caab",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": []
   },
   "outputs": [],
   "source": [
    "integration_times = [\n",
    "    0.02,\n",
    "    0.04,\n",
    "    0.06,\n",
    "    0.08,\n",
    "    0.1,\n",
    "    0.2,\n",
    "    0.3,\n",
    "    0.4,\n",
    "    0.5,\n",
    "    0.6,\n",
    "    0.7,\n",
    "    0.8,\n",
    "    0.9,\n",
    "    1.0,\n",
    "    1.1,\n",
    "    1.2,\n",
    "    1.3,\n",
    "    1.4,\n",
    "    1.5,\n",
    "    1.6,\n",
    "    1.7,\n",
    "    1.8,\n",
    "    1.9,\n",
    "    2.0,\n",
    "    2.1,\n",
    "    2.2,\n",
    "    2.3,\n",
    "    2.4,\n",
    "    2.5,\n",
    "    2.6,\n",
    "    2.7,\n",
    "    2.8,\n",
    "    2.9,\n",
    "    3.0,\n",
    "    3.1,\n",
    "    3.2,\n",
    "    3.3,\n",
    "    3.4,\n",
    "    3.5,\n",
    "    3.6,\n",
    "    3.7,\n",
    "    3.8,\n",
    "    3.9,\n",
    "    4.0,\n",
    "    4.1,\n",
    "    4.2,\n",
    "    4.3,\n",
    "    4.4,\n",
    "    4.5,\n",
    "    4.6,\n",
    "    4.7,\n",
    "    4.8,\n",
    "    4.9,\n",
    "    5.0,\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1345e80b-a773-4c50-aca7-b8a6a9fa0e7f",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(detector.data[\"/statistics/pixel/mean\"], integration_times)\n",
    "plt.xlabel(\"Integration time [s]\")\n",
    "plt.ylabel(\"Pixel mean\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39463414-0aa8-4b66-863a-7db81365d0fa",
   "metadata": {},
   "source": [
    "### Signal-to-noise ratio example\n",
    "The model ```signal_to_noise_ratio``` computes the mean and the variance of the input array and calculates the ratio of that for each time step."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a1879149-b578-4ffb-be0d-6c17e5a9c8b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "detector.data[\"/snr/signal\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6af905b1-2d82-41ba-bc79-440dd11f2928",
   "metadata": {},
   "source": [
    "### Mean-variance model example\n",
    "The model ```mean_variance``` was developed to do a quick and easy Photon-Transfer curve (PTC) analysis, where you need the calculated mean and variance of the image. It computes a mean-variance 1D array that shows relationship between the mean signal of a detector and its variance for a selected data_structure (default is \"image\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ce26fcb6-4ce7-49dd-bbac-231cea249afb",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_variance = detector.data[\"/mean_variance/image/variance\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a5dd545-4d3d-4e72-b513-6f788ea943b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_variance.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b3c9ee37-613e-4f91-b22b-a97cb7b03db1",
   "metadata": {},
   "source": [
    "### Linear regression model example\n",
    "The linear regression model was developed for quick and easy linear regression analysis of the simulation. It computes a linear regression along readout time for a selected data_structure (default is \"image\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5dbba56d-d548-4120-9cfb-9c8c3f913e1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ca604d00-8274-4480-94bc-55d66c7e923c",
   "metadata": {},
   "source": [
    "Plotting the intercept of the pixel shows the illumination level per pixel computed along the time. Illumination level is 5000 e-/s (see YAML config file).\n",
    "\n",
    "When including more noise with enabling more models, the intercept values go down respectively:\n",
    "\n",
    "- Simple dark current with dark_rate: 20 e-/pixel/second -> reduces intercept by factor of 20.\n",
    "- Loading a PRNU file -> reduces intercept "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ca3ea716-c197-40e8-87f9-b6ce1a0554e6",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[\"/data/linear_regression/pixel/intercept\"].plot(robust=True, yincrease=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2c15fe3-da36-43a1-92bd-866edb516ccd",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "prnu = np.load(\"data/noise.npy\")\n",
    "plt.figure(100000)\n",
    "np.shape(prnu)\n",
    "plt.imshow(prnu)"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

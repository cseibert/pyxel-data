{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "53c09fec-c65c-487e-a9ec-7f5a36b95a09",
   "metadata": {},
   "source": [
    "# Dark current vs temperature for MCT detectors\n",
    "\n",
    "##  Author\n",
    "2022&mdash;Constanze Seibert\n",
    "\n",
    "## Keywords\n",
    "dark current, observation mode, temperature, MCT detector\n",
    "\n",
    "## Learning Goals\n",
    "In this notebook we use Pyxel in observation mode and visualize the results of the dark current versus temperature for rule07.\n",
    "If one or more values for the fixed pattern noise factor are given it will include the Dark Signal Non Uniformity (DSNU).\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`observation_product_mode`  | Necessary | Background |\n",
    "| [Dask/Distributed](https://distributed.dask.org/en/stable/quickstart.html) | Helpful | |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "| [matplotlib](https://matplotlib.org) | Helpful | |\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in observation mode by changing the following parameters:\n",
    "1. Temperature from detector operating temperature up to room temperature in K\n",
    "2. Dark current spatial noise factor is typically between 0.1 and 0.4.\n",
    "\n",
    "Check also the [documentation](https://esa.gitlab.io/pyxel/doc/latest/references/model_groups/charge_generation_models.html#dark-current).\n",
    "\n",
    "<div class=\"alert alert-block alert-info\">\n",
    "    <b>Tip:</b> Open the <i>observation_rule07.yaml</i> file in parallel for a better overview.</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a505cd0d-09ca-4b85-badc-a51702bfa357",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3a4ef7c0-99ec-4732-8d99-ebfc5f4829a3",
   "metadata": {},
   "source": [
    "## Loading the configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7d19178-a16b-4af6-83d5-33178ad56cc3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a configuration class from the yaml file\n",
    "config = pyxel.load(\"observation_rule07.yaml\")\n",
    "\n",
    "observation = config.observation  # class Observation\n",
    "detector = config.detector  # class CMOS\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "957dc166-e3fc-4942-98d5-f299cee31538",
   "metadata": {},
   "source": [
    "## Running the pipelines"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "283dd9a3-4310-40a1-bcec-6b87c604b38e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5e143a5b-e0fb-4616-af16-5b8791fdee8c",
   "metadata": {},
   "source": [
    "## Plotting the outputs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4fef672-0d19-45a4-aec3-b9eddcb72819",
   "metadata": {},
   "source": [
    "The plot will show the dark current model rule 07 used in Pyxel against the temperature for different fixed pattern noise (FPN) values. The dark current figure of merit at 300 K is 0.01 nA/cm^2.\n",
    "Higher FPN values result in higher dark current values, while all calculations converge for temperatures higher than 180 K.\n",
    "\n",
    "You can set temporal noise to **true** in the yaml file, run everything again and see the difference in the plot, if temporal noise is included. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e02308d2-95d6-40ee-9c42-ab5a505b23ec",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a72824c-03e6-46a7-96d0-9eb9da7c703f",
   "metadata": {},
   "outputs": [],
   "source": [
    "dark_current = result.pixel.mean(dim=[\"x\", \"y\"]).to_numpy()\n",
    "temperature = result.temperature.to_numpy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "439dc9db-a0b0-4aef-97a9-aef0346385c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.figure(1)\n",
    "for i in range(0, len(result.spatial_noise_factor)):\n",
    "    plt.semilogy(\n",
    "        temperature,\n",
    "        dark_current[i],\n",
    "        \"-\",\n",
    "        ms=5,\n",
    "        mfc=\"none\",\n",
    "        label=f\"FPN factor: {result.spatial_noise_factor.values[i]}\",\n",
    "    )\n",
    "plt.title(\"Dark current vs. temperature\")\n",
    "plt.xlabel(\"Temperature [K]\")\n",
    "plt.ylabel(\"Dark current [e-/pixel/s]\")\n",
    "plt.xlim([145, 200])\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ff26018-791b-4f2f-8de7-8ef58bfd62c4",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time, temperature and figure of merit\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported.\n",
    "\n",
    "You can use the toolbar to change temperature and figure of merit to see the influence on the detector.\n",
    "\n",
    "Try and change the readout time in the configuration file, reload the configuration and run the pipeline again. Then display the results and see the influence of the readout time.\n",
    "\n",
    "Longer readout times result in higher dark current and saturation is reached at lower temperatures already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bf778cc-684c-420f-8df7-324068bf580e",
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1b5097e-fc21-4f35-92ec-edf7b310feae",
   "metadata": {},
   "outputs": [],
   "source": [
    "out_1a = hv.Dataset(result[\"image\"])\n",
    "plot_1a = out_1a.to(hv.Image, [\"x\", \"y\"], dynamic=True)\n",
    "plot_1a.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"]))"
   ]
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": true,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

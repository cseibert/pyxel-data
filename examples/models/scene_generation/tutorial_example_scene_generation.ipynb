{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "6efc44dd-3ec4-4c67-9809-e2242ccd9f91",
   "metadata": {},
   "source": [
    "# Scene generation and projection\n",
    "\n",
    "## Author\n",
    "2023&mdash;Constanze Seibert & Frederic Lemmel\n",
    "\n",
    "## Learning goals\n",
    "In this notebook you will learn how to use two new models \n",
    "- [```load_star_map```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/scene_generation_models.html#load-star-map) to generate a scene \n",
    "\n",
    "Generates a scene from scopesim Source object loading objects from the GAIA catalog for given coordinates and FOV.\n",
    "\n",
    "and \n",
    "- [```simple_aperture```](https://esa.gitlab.io/pyxel/doc/stable/references/model_groups/photon_collection_models.html#simple-aperture) to project this scene onto your detector. \n",
    "\n",
    "Converts scene to photon with given aperture. First an xarray Dataset will be extracted from the Scene for a selected wavelength band, where the flux of the objects will be integrated along the wavelength band. This integrated flux in photon/(s cm2) is converted to photon/(s pixel). Finally, the objects are projected onto detector, while converting the object coordinates from arcsec to detector coordinates (pixel).\n",
    "\n",
    "\n",
    "Both models are are implemented in Pyxel since version 1.11.\n",
    "\n",
    "## Keywords\n",
    "scene generator, star map, flux conversion\n",
    "\n",
    "## Summary\n",
    "We use Pyxel in exposure mode and generate a scene from the Gaia catalog with a virtual telescope pointing towards the Pleiades with a FOV diameter of $0.5^{\\circ}$. We visualise some selected spectra and the scene in arcsec. Then this scene is projected onto the detector using a simple aperture and a PSF model."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8c8205a5-9d2a-4ce8-be13-446c1d7d9014",
   "metadata": {},
   "source": [
    "#### Loading packages "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4567663-f4eb-4ada-ba20-bf51a1876847",
   "metadata": {},
   "outputs": [],
   "source": [
    "%matplotlib inline\n",
    "import matplotlib.colors as colors\n",
    "import matplotlib.pyplot as plt\n",
    "import xarray as xr\n",
    "\n",
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30b5ac1c-bd90-4180-b2c7-035baad83d19",
   "metadata": {},
   "source": [
    "#### Load configuration file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3089eb0-0807-4f5b-86a6-7829b5a3bcc2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cfg = pyxel.load(\"tutorial-scene.yaml\")\n",
    "\n",
    "detector = cfg.detector"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c71b975c-7741-4b55-bc18-e10c886bd9c3",
   "metadata": {},
   "source": [
    "#### Loading result"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b99ce01-b8d0-49e2-bc0a-cfe192ce776e",
   "metadata": {},
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(mode=cfg.exposure, detector=detector, pipeline=cfg.pipeline)\n",
    "\n",
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13866a9d-fe55-4601-8b15-0b050a270a86",
   "metadata": {},
   "source": [
    "### Get Scene from DataTree"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffb89ccd-8583-4fbf-accd-32a96bf9de39",
   "metadata": {},
   "source": [
    "Info: Detector object contains only last exposure (time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "976e117d-247f-45b7-b465-355b919ccfde",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_tree = detector.scene.data\n",
    "\n",
    "data_tree[\"/list/0\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6a817b9e-a962-4e9b-a747-e6612413ccaf",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result.scene\n",
    "result[\"scene/list/0\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "107230d8-ab6a-47ba-b5cc-e658d99c1f18",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Turn DataTree to Dataset\n",
    "For now needed, because plotting not available in DataTree yet."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8eaad7ed-7259-4934-a98f-7ea782fb9e82",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ds = data_tree[\"/list/0\"].to_dataset()\n",
    "\n",
    "ds"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db2d58c1-b8b4-495e-a4da-d251a5198bbe",
   "metadata": {},
   "source": [
    "### Visualize spectra of selected stars"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a97fd943-e6bc-4733-a4fe-2b8a40cf690a",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"flux\"].isel(ref=slice(None, 9)).plot.line(col=\"ref\", sharey=False, col_wrap=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fe6dd44-aa9d-412b-8e00-9033b2b6096b",
   "metadata": {},
   "outputs": [],
   "source": [
    "ds[\"flux\"].isel(ref=100).plot.line()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e54a42ba-2159-4f38-b4ec-86e70d68cff5",
   "metadata": {},
   "source": [
    "### Visualize scene with magnitude as intensity scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0b3bb0b-adf5-40f3-a52f-ed3e0e68d99f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "ds.plot.scatter(x=\"x\", y=\"y\", hue=\"weight\", marker=\"o\", size=8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6dccac33-bb5e-4b82-b91f-30068b03ebaf",
   "metadata": {},
   "source": [
    "## Display detector "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09abc18c-a878-482c-b38b-d3ef636ebfe6",
   "metadata": {},
   "source": [
    "The new features in the function ```display_detector``` will be part of the next release 1.12 (coming soon)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d942be6-f3f1-40b4-b1d1-1a4ee9c703b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8de145c8-ce92-4409-bba1-5d2e7e4c3d86",
   "metadata": {},
   "source": [
    "Alternative to display_detector to visualize photon and image array in logarithmic scale"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81dcf4ad-836d-4890-b9a3-2063541fa404",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.DataArray(result.photon).plot(\n",
    "    norm=colors.LogNorm(\n",
    "        vmin=0.1,  # 1\n",
    "        vmax=result.photon.max(),\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6201987a-5a8e-4518-bb51-1b431c6b48c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "xr.DataArray(result.image).plot(\n",
    "    norm=colors.LogNorm(\n",
    "        vmin=1,  # 0.1\n",
    "        vmax=result.image.max(),\n",
    "    )\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "569a0468-90a7-4545-9325-7d9676256ce8",
   "metadata": {},
   "source": [
    "### TO DOs:\n",
    "We have a first nice working version, but still want to improve the models.\n",
    "See https://gitlab.com/esa/pyxel/-/issues/668."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8b9ef49-b4cf-4cf1-93f5-de4b1ad9d3b8",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "```{warning}\n",
    "\n",
    "Package `poppy` (see https://pypi.org/project/poppy/) must be installed !\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "(first_simulation)=\n",
    "# First simulation with Pyxel\n",
    "\n",
    "<img style=\"float: right;\" class=\"yolo\" src=\"../images/pyxel_logo.png\" width=\"250\">\n",
    "                                                                     \n",
    "## Authors\n",
    "\n",
    "[The Pyxel development team](mailto:pyxel@esa.int)\n",
    "\n",
    "## Keywords\n",
    "\n",
    "Exposure mode\n",
    "\n",
    "## Prerequisites\n",
    "\n",
    "| Concepts | Importance | Notes |\n",
    "| -------- | ---------- | ----- |\n",
    "| {ref}`intro_install_pyxel`  | Necessary | Background |\n",
    "| [xarray](https://tutorial.xarray.dev/intro.html) | Helpful | Background |\n",
    "| [bokeh](https://bokeh.org) | Helpful | |\n",
    "\n",
    "## Learning Goals\n",
    "By the end of the lesson you will know how to:\n",
    "* Load the configuration file\n",
    "* Run Pyxel in exposure mode from the Jupyter notebook\n",
    "* Display the final detector object\n",
    "* Save the outputs\n",
    "\n",
    "## Summary\n",
    "\n",
    "In this notebook we will see Pyxel in action by running a simple simulation using the **exposure mode**! We will apply a number of models to a `.fits` image of Pleiades. All the necessary configuration is provided in the file `exposure.yaml`.\n",
    "\n",
    "We will use Pyxel's module `configuration` to load the configuration file and functions `exposure_mode` and `display_detector` from modules `run` and `notebook`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import pyxel"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load configuration\n",
    "\n",
    "The main input of a Pyxel simulation is the `YAML` configuration file, specifying information about the running mode, the detector and all the models user wants to apply (the pipeline). Configuration file is in the `YAML` format and it is loaded with the function `load()`, which outputs an instance of class `Configuration`, in our case the `config` object. \n",
    "\n",
    "Feel free to check out the configuration file! For example, you will find the path to the input `.fits` file as an argument of the `load_image` model under `pipeline -> photon_generation`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "config = pyxel.load(\"exposure.yaml\")  # class Configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Create running mode, detector and pipeline objects\n",
    "\n",
    "By inspecting the configuration file, one can see that it is separated into three main compartments, each representing a class in the Pyxel architecture (`Exposure`, `CCD`, `DetectionPipeline`). They are saved as attributes in the configuration object and we can access them in the following way:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "exposure = config.exposure  # class Exposure\n",
    "detector = config.ccd_detector  # class CCD\n",
    "pipeline = config.pipeline  # class DetectionPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run the pipeline\n",
    "\n",
    "We can run the single mode simulation with the function `exposure_mode()`, passing the objects `exposure`, `detector` and `pipeline`. By doing so, the `detector` object passes through the pipeline once where it is edited by the models. By default the data is read out once at the readout time of 1 second."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result = pyxel.run_mode(\n",
    "    mode=exposure,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")\n",
    "\n",
    "result"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What happens to the detector object during the pipeline is easiest to explain with an image. We can see below that the detector stores its properties as well as data representing different stages of the imaging process. These are `Photon`, `Pixel`, `Signal` and `Image` arrays as well as `Charge`, which is a Pandas dataframe representing the charge point cloud. `Data` is a Xarray Dataset containing the processed data from the models in that group. The buckets are edited by the functions in the pipeline - models.\n",
    "\n",
    "<img src=\"../images/architecture.png\" width=\"500\">\n",
    "\n",
    "You can now check out the output folder for saved data.\n",
    "\n",
    "## Display detector\n",
    "\n",
    "We can display the detector at the end with function `display_detector()`, which shows us different arrays stored inside."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "pyxel.display_detector(detector)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-info\">Charge is not part of this display since it is a dataframe of a charge cloud in the detector volume and not an array.</div>\n",
    "\n",
    "You can also find the saved outputs that were specified in the configuration file in the output folder.\n",
    "\n",
    "To get to know the YAML configuration and Pyxel classes a bit more in depth, continue with the next step of the tutorial: [Pyxel configuration and classes](02_pyxel_configuration_and_classes.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accessing data stored inside the `xarray` dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result[\"image\"].sel(time=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To convert to a numpy array, method `to_numpy` can be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result[\"image\"].sel(time=1).to_numpy()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Xarray datasets also support math operations:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "result[\"image\"].sel(time=1).sum()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting image as function of readout time\n",
    "\n",
    "Library `holoviews` can be used to quickly plot data stored inside `xarray` datasets.\n",
    "Here with the `bokeh` backend, `matplotlib` is also supported."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Display one 'image'\n",
    "result[\"image\"].sel(time=1).plot()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "# Display all 'images'\n",
    "result[\"image\"].plot(col=\"time\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts\n",
    "\n",
    "hv.extension(\"bokeh\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "out = hv.Dataset(result[\"image\"])\n",
    "image = out.to(hv.Image, [\"x\", \"y\"], dynamic=True)\n",
    "plot = image.opts(opts.Image(aspect=1, cmap=\"gray\", tools=[\"hover\"])).opts(\n",
    "    framewise=True, axiswise=True\n",
    ")\n",
    "\n",
    "plot"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting image with `xarray`\n",
    "\n",
    "It's possible to plot directly using library `xarray`. \n",
    "See here for more information: https://docs.xarray.dev/en/stable/user-guide/plotting.html"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "toc-autonumbering": false,
  "toc-showmarkdowntxt": false,
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

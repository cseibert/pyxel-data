# Welcome to the Pyxel Tutorials and Examples !

**[`Pyxel`](https://esa.gitlab.io/pyxel) is a general detector simulation framework.**

We recommend you to start with the tutorial available in the form of Jupyter notebooks. 
It covers all the basics, the four running modes and adding a new model. 
Apart from the tutorial, more examples on running modes and different models are also available. 
See below for a full list.

All tutorials and examples can be found in this public repository.
Please note, that this Jupyter book page is just for viewing the examples and not interactive.

If you want to change the inputs and see the results immediately,
you need to a install Pyxel and download the [Pyxel Data repository](https://gitlab.com/esa/pyxel-data) or launch a live session on Binder.

Once you’ve installed Pyxel, the example repository can be either downloaded directly by clicking on button download
or using Pyxel by running the command:

```bash
pyxel download-examples
```

Now you can launch JupyterLab to explore them:

```bash
cd pyxel-examples

jupyter lab
```

You can run also tutorials and examples without prior installation of Pyxel in a live session here: 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab)


```{tableofcontents}
```

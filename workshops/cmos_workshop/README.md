# CMOS Workshop 2022

```{tableofcontents}
```

*Workshop at the CMOS Workshop at ESTEC, The Netherlands*

* **DATE**: Friday November 24th, 2022
* **TIME**: 9:00 to 12:00
* **LOCATION**: ESTEC, The Netherlands

## PRE-WORKSHOP SETUP

Please be sure your laptop is properly configured before the workshop by following the [installation and setup instructions](https://esa.gitlab.io/pyxel/doc/stable/tutorials/overview.html#quickstart-setup).

As an alternative, a workshop session can be run on mybinder.org via this link: [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/esa%2Fpyxel-data/HEAD?urlpath=lab)

## Agenda

| Time | Topic                                                                                                                                                                                        |
| ---- |----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 09:00 - 9:30 | [Installing Pyxel and getting the tutorials to work](https://esa.gitlab.io/pyxel-data/workshops/cmos_workshop/1_overview/installation.html)                                                  |
| 09:30 - 9:50 | [Overview and "my first Pyxel simulation" tutorial ](https://esa.gitlab.io/pyxel-data/use_cases/leiden_university_workshop/first_simulation.html)                                            |
| 09:50 - 10:10 | [Observation mode tutorial featuring a dark current versus temperature simulation](https://esa.gitlab.io/pyxel-data/workshops/cmos_workshop/2_observation_dark_current/dark_current_Si.html) |
| **10:10 - 10:25** | **Break**                                                                                                                                                                                    |
| 10:25 - 10:55 | [Calibration mode tutorial featuring a CCD CTI model fit to lab test data](https://esa.gitlab.io/pyxel-data/workshops/cmos_workshop/4_calibration_cti/calibration_cti.html)                  |
| 10:55 - 11:05 | ["Adding your brand new model to Pyxel" tutorial](https://esa.gitlab.io/pyxel-data/workshops/cmos_workshop/5_adding_new_model/create_model.html)                                             |
| 11:05 - 11:15 | Contribution tips                                                                                                                                                                            |
| 11:15 - 12:00 | Time allocated to specific model demonstrations - please let us know if you have any wishes                                                                                                  |

## Description

In this training you will receive support in installing Pyxel on your own computer, follow a step-by-step tutorial on running a first detector simulation, get familiar with the different modes of operation, learn how to add your own models and contribute to the framework.

If you would like to get a head start with the tools we will be concentrating on you can check out their documentation:

* [Documentation](https://esa.gitlab.io/pyxel/doc/stable)
* [Blog](https://esa.gitlab.io/pyxel)
* [Gitter](https://gitter.im/pyxel-framework/community)
* [Google Group](https://groups.google.com/g/pyxel-detector-framework)

## Problems or Questions ?
We encourage you to submit any problems or questions you have to this
repository [issue tracker](https://gitlab.com/esa/pyxel/-/issues)

{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "231f022b-bb0b-4c6e-8999-0e677a8de471",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Charge transfer ineffiency"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4520089-57ad-49f1-8a78-eeefc4d3abc3",
   "metadata": {},
   "source": [
    "## Author\n",
    "2022&mdash;Bradley Kelman\n",
    "\n",
    "## Learning Goals\n",
    "* use of calibration mode\n",
    "* simulation of Charge-Transfer Inefficiency\n",
    "\n",
    "## Keywords\n",
    "Charge-Transfer Inefficiency (CTI), charge injection\n",
    "\n",
    "## Summary\n",
    "This notebook gives an example of the use for calibration mode, it will explore the effect of charge transfer inefficiency (CTI) on 1D charge injection profiles. It will attempt to find the correct trap parameters (trap release timescales and trap density) of a damaged profile that contains CTI. The input (the profiles the Pyxel uses to simulate the CTI effect) will be CTI free charge profiles and the target data (the profiles Pyxel uses for comparison to obtain a fitness value) will be the CTI damaged profiles.  "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7749abb4-c272-4cf4-8ea6-94a1d0dd4314",
   "metadata": {},
   "source": [
    "## Calibration mode\n",
    "\n",
    "Calibration mode in Pyxel is designed for finding and optimising given arguments of a model. The calibration mode itself takes in two images, the first is an input profile which is free of the detector effect that needs to be simulated and second is the target image, this is an image that contains the desired detector effect. The calibration mode will simulate the desired detector effect multiple times on the input profile in an attampt to get its resultant simulated image to resemble the target image as close as possible. An image of how the simulation works is seen below. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "943d7da3-18e4-463e-9b08-1f361803578f",
   "metadata": {},
   "source": [
    "![Calibration](images/calibration.png) Figure 1 - Overview of Pyxels calibrations mode "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51e5e3e2-ed35-4260-b18d-b4cb8d9e3690",
   "metadata": {},
   "source": [
    "## Initialization"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "566e6b83-96a2-49e2-8e10-a504004643f5",
   "metadata": {},
   "source": [
    "Import required packages"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d0d70f5-cfad-410f-9c8a-7619493d8391",
   "metadata": {},
   "outputs": [],
   "source": [
    "import dask\n",
    "import numpy as np\n",
    "import pyxel\n",
    "from pyxel.notebook import (\n",
    "    champion_heatmap,\n",
    "    display_evolution,\n",
    "    display_simulated,\n",
    "    optimal_parameters,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3938d35c-a3c6-419c-87f4-8535c2b4fa56",
   "metadata": {},
   "source": [
    "Check Pyxel version "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c20c461d-4cae-4168-b2a7-46177b9a46be",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Display current version of Pyxel\n",
    "\n",
    "print(\"Pyxel version:\", pyxel.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "774956d7-314e-4989-bc18-5b2514d062a7",
   "metadata": {},
   "source": [
    "### Create a ``Dask`` cluster\n",
    "\n",
    "Used for the parallisation of calibration mode"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7114842d-ac9a-493a-b9c4-c9fbefbeebaa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a 'local' Cluster\n",
    "from distributed import Client\n",
    "\n",
    "client = Client()\n",
    "\n",
    "client"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27446590-b798-48fa-a2bc-d8af1e696ba5",
   "metadata": {},
   "source": [
    "Defining the input and target filenames to be used in the calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bf239ee-b058-4d9c-bbb7-5373b6daa49e",
   "metadata": {},
   "outputs": [],
   "source": [
    "input_filenames = [\n",
    "    \"profiles/input/r_G0_0.txt\",\n",
    "    \"profiles/input/r_G0_10.txt\",\n",
    "    \"profiles/input/r_G0_20.txt\",\n",
    "    \"profiles/input/r_G0_30.txt\",\n",
    "    \"profiles/input/r_G0_40.txt\",\n",
    "    \"profiles/input/r_G0_50.txt\",\n",
    "    \"profiles/input/r_G0_60.txt\",\n",
    "    \"profiles/input/r_G0_70.txt\",\n",
    "    \"profiles/input/r_G0_80.txt\",\n",
    "    \"profiles/input/r_G0_90.txt\",\n",
    "    \"profiles/input/r_G0_100.txt\",\n",
    "    \"profiles/input/r_G0_110.txt\",\n",
    "    \"profiles/input/r_G0_120.txt\",\n",
    "]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0000c44b-3590-423f-908a-46a21e4f5c7d",
   "metadata": {},
   "outputs": [],
   "source": [
    "target_filenames = [\n",
    "    \"profiles/target/CTIr_G0_0.txt\",\n",
    "    \"profiles/target/CTIr_G0_10.txt\",\n",
    "    \"profiles/target/CTIr_G0_20.txt\",\n",
    "    \"profiles/target/CTIr_G0_40.txt\",\n",
    "    \"profiles/target/CTIr_G0_30.txt\",\n",
    "    \"profiles/target/CTIr_G0_50.txt\",\n",
    "    \"profiles/target/CTIr_G0_60.txt\",\n",
    "    \"profiles/target/CTIr_G0_70.txt\",\n",
    "    \"profiles/target/CTIr_G0_80.txt\",\n",
    "    \"profiles/target/CTIr_G0_90.txt\",\n",
    "    \"profiles/target/CTIr_G0_100.txt\",\n",
    "    \"profiles/target/CTIr_G0_110.txt\",\n",
    "    \"profiles/target/CTIr_G0_120.txt\",\n",
    "]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1af40d49-3fa7-4a61-ad48-5c0dab5e7c3a",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Creating target data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b16226b7-d0f0-4756-920a-f0d126204938",
   "metadata": {},
   "source": [
    "### Explanation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2b7c47f-bfca-4040-9a86-f309c1c99555",
   "metadata": {},
   "source": [
    "Pyxels calibration mode can optimise given argumentes using multiple input and target images (as long as both contain the same number of images), this has uses if there are multiple images with different exposure times or as the case in this example there are multiple signal values on the same detector. The fitness values obtained in this case will be based upon the sum of the residuals from each image given."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e41efa91-d86c-462e-b19f-0688b42d8cc5",
   "metadata": {},
   "source": [
    "### Observation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddb854e8-7265-471a-b95e-5475a9d62e75",
   "metadata": {},
   "source": [
    "The best way to produce multiple charge injection profiles from out input profiles is to use Pyxels observation mode which will simulated the addition of CTI to each charge profile sequentially. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "65ed685c-9efb-421b-942f-87395c6eaa55",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"yaml_files/cdm_observation.yaml\")\n",
    "\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline\n",
    "observation = config.observation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0afc9ef9-6480-44c8-a592-8ea09eacc736",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "result = pyxel.observation_mode(\n",
    "    observation=observation,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a1a218b-2885-4a24-aa82-e0025a46d597",
   "metadata": {},
   "source": [
    "The first element of the result shows the outputs in the form of an xarray, this is where the simulated profiles are stored - we want the signal value in electrons hence we will select the 'pixel' data variable for each signal level used and save them for the calibration to use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5ab859af-8221-4c00-aec9-311e91f6db6c",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b46f5a7-5495-4612-a1b0-b686ed29db53",
   "metadata": {},
   "outputs": [],
   "source": [
    "result[0][\"pixel\"].plot(hue=\"filename_id\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0b69cc9-9f0c-4b5e-9fba-028727be111c",
   "metadata": {},
   "outputs": [],
   "source": [
    "for i in range(13):\n",
    "    np.savetxt(\n",
    "        target_filenames[i], result[0][\"pixel\"].sel(filename_id=i).squeeze().values\n",
    "    )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10572119-7e6c-4326-8ba6-f4b854cb3583",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Calibration simulations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64e7fba9-b931-48ae-bde0-6d692fa46646",
   "metadata": {},
   "source": [
    "### How calibration mode works"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "255e7fa2-a5d2-4491-ba36-c1024e6f3bcf",
   "metadata": {},
   "source": [
    "#### Overview"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e143f5f-42d2-48f5-91bc-8fed2a680e32",
   "metadata": {},
   "source": [
    "Initially a random set of values are asigned to the arguments that are to be optimised, these values are then used to simulate the desired detector effect on the input image, producing a simulated iamge. This simulated image is then compared to the target image with the fitness value (Pyxels goodness of fit term) based upon the residual values between the simulated and target image. Once a fitness value is given the values of the arguments are mutated using a genetic algorithm approach and simulated again in the hope of getting a lower fitness value. As the parameter space is explored the calibration modes fitness value converges to a minima. The arguemnt values that give this fitness minima are the calibrations modes answer to the values of the particular detector effect seen in the target image."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79078ff3-c137-4c74-a1bf-a8b04fd48358",
   "metadata": {},
   "source": [
    "#### Effect of calibration parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3936b75e-7d6a-491a-a74c-57bcce561930",
   "metadata": {},
   "source": [
    "The calibration has multiple parameters that can change the size and computational resources used for the simulation. \n",
    "\n",
    "Population size dictates the number of individual pipelines are run at a time. All individuals within a given population are known as a generation, once all individuals within a generation have been simulated their fitness values are compared, with the best being mutated and used in the following generation, with this number being dictated by the best_decisions parameter. Once the set number of generations is reached, that marks the end of a single evolution. The lowest fitness value found once the given number of evolutions has been reached will be the best answer found by the calibration.\n",
    "\n",
    "It is also possible to parallelise calibration mode with the use of the island parameter, each island is run seperately and at the end of an evolution its best fitness values are compared to the best fitness values found by the other islands, the resulting best arguemnts are then taken into the proceeding evolutions for each island and mutated. It is a way of investigating the parameter space far quicker.\n",
    "\n",
    "If you wish to run a large number of islands it is recommended for optimal use of computation resources to use one thread per worker:\n",
    "\n",
    "\n",
    "`client = Client(threads_per_worker=1)`\n",
    "\n",
    "This way an entire thread can be dedicated to a single island, be careful though as the number of islands will be limited by the number of threads within the CPU of the machine you are using. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "02c5a63c-a73d-475a-b3e9-356b20ad96e3",
   "metadata": {},
   "source": [
    "The following figures will help explain how each calibration parameter works within the calibration mode. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "136a3d9c-339b-414f-b12a-bac7dc6be768",
   "metadata": {},
   "source": [
    "##### Calibration parameter figures"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b86caad7-4471-43cf-8c30-37d149066286",
   "metadata": {},
   "source": [
    "The figure below shows what an individual looks like, it is a single exposure pipeline run that applies a set of trap parameters to the input data, this then produces an image with synthetically induced CTI. This image is then compared to the target data with the residuals of which are used to find a fitness value which is used to determine the accuracy of the trap parameters used for this specific pipeline run."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "686af20b-d162-4177-82eb-ef84ac923725",
   "metadata": {},
   "source": [
    "![Calibration](images/Pipeline_calibration.png) Figure 2 - Example of an 'individual'; a single pipeline run by pyxel."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d2f5d7d-1139-4ea1-af64-ac62920210cf",
   "metadata": {},
   "source": [
    "Below is the next stage in the calibration mode, it defines most of the parameters used to simulate the calibration. $I_{n}$ represents the island number, and $tp_{n}$ the trap properties f a given individual within a population $P_{n}$ and a given generation $G_{n}$ each individual with have a fitness value associated with it $F_{n}$ with the trap parameters from the best fitness values of the generation being mutated to form the new set of trap parameters $tp_{n}^{*}$. After the defined number of generation is reache the parameters that have the best fitness is the champion for the given evolution. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aede4b16-953b-4d19-a8ff-95647525fce9",
   "metadata": {},
   "source": [
    "![Calibration](images/Evolution_calibration.png) Figure 3 - Example of an entire evolution."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08cf89ea-a7d7-4f71-9c55-64984758755f",
   "metadata": {},
   "source": [
    "Evolutions can also be chained in series and in parallel (if enough computation threads are present in the machine). The champion fitness values of evolution$_{n}$ are compared between islands to explore the parameter space quicker and more evolutions are run afterwards with mutated versions of the champion parameters found from this comparison. Once the given number of evolutions are complete the final trap parameters found to have the best fitness value is the champion parameter of the entire calibration simulation. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a48f9119-3678-4b75-9cab-3b28a598206b",
   "metadata": {},
   "source": [
    "![Calibration](images/Full_calibration_simulation.png) Figure 4 - Overview of the entire calibration simulation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0c462aa-a3b2-494a-8714-bd94ff1c5258",
   "metadata": {},
   "source": [
    "### Calibration simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3160487e-1d8c-4c4c-820e-046d4ea8b5b9",
   "metadata": {},
   "source": [
    "Similar to the observation mode above, the yaml file has to be loaded and instanciated before the calibration can be run. Once loaded a timestamp is created and this is the directory under which the resulting simulation datasets will be saved. A handy tip is to make sure the directory stated in the yaml file is clear so you know where the data is saved. If you want to know the directory the simulation saves to specifically you can timestamp just before the yaml file is laoded and this should correlate closely with the name of the directory."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d1688404-436b-4051-9739-c0006b1f9079",
   "metadata": {},
   "source": [
    "To start, an exmaple is given that uses relatively little computation resources, simulate these first and then play around with the calibration parameters to get a feel for how long a simulation takes and how well optimised the parameters can become."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e63bf74e-6c67-4c43-a882-38cce8699545",
   "metadata": {},
   "source": [
    "Load yaml file and instinciate the detector, pipeline and calibration"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bcfc6477-287e-4a05-bed8-e404cbf8727e",
   "metadata": {},
   "outputs": [],
   "source": [
    "config = pyxel.load(\"yaml_files/cdm_calibration.yaml\")\n",
    "\n",
    "detector = config.detector\n",
    "pipeline = config.pipeline\n",
    "calibration = config.calibration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12565d34-b315-4ef1-83ca-b9fe010e6864",
   "metadata": {},
   "source": [
    "The parameters given in the yaml file are loaded but can also be overwritten if needed. Below is an example of how to overwrte the calibration parameters."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "295b1ca8-0300-48df-b773-830492897eab",
   "metadata": {},
   "outputs": [],
   "source": [
    "config.calibration.algorithm.population_size = 20\n",
    "config.calibration.algorithm.generations = 20\n",
    "config.calibration.num_best_decisions = 5\n",
    "config.calibration.num_evolutions = 2\n",
    "config.calibration.num_islands = 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "09c91586-3d08-48fa-8eda-f134de3e96ef",
   "metadata": {},
   "source": [
    "The cell below runs the main calibration simulation. First the islands are set up, once this is completed the main calibration simulation is run."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9cc79b9-0fd9-4e87-9bb3-360c5b288b4c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "result = pyxel.run_mode(\n",
    "    calibration,\n",
    "    detector=detector,\n",
    "    pipeline=pipeline,\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c083d826-e184-4bb1-b1f8-b434f8943ae8",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Data analysis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fcbb27b2-bea9-4174-ac33-ecb0c490352e",
   "metadata": {},
   "outputs": [],
   "source": [
    "result"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d8c98f9-7e3c-4543-988c-1b4565e56323",
   "metadata": {},
   "source": [
    "The result of the calibration takes the form of an xarray dataset. It can be manipulated as the user sees fit and can be analysed in a number of ways. A few examples of analysis tools that pyxel has is shown below"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fa6ffaaa-b8b0-44af-af07-1508b17e098d",
   "metadata": {},
   "source": [
    "#### Display evolution"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "948bc965-eef7-4e6f-8737-1af6c167982d",
   "metadata": {},
   "source": [
    "Dispaly_evolution provides a graph of the fitness value against the number of evolutions. The island and param_id (which in this case selects a specific charge profile from the list of charge profiles used) can be changed to view each combination of island and parameter simluated. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f7d83a81-bae9-4bd7-813e-4dc2b3c79d34",
   "metadata": {},
   "outputs": [],
   "source": [
    "result.load()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb8ff2d0-10f9-42ce-83b6-6764c38fd574",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_evolution(result[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0aa17df-d920-4077-8d48-22b96c215902",
   "metadata": {},
   "source": [
    "#### Optimal parameters"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70eb69b4-eda7-4059-8cfb-833cb20a89ca",
   "metadata": {},
   "source": [
    "The optimal parameters show the results that give the best fitness value from the entire calibration. It also shows which island and evolution gave that result. \n",
    "\n",
    "The champion parameters are in the order specified within the yaml file, for example in the yaml file used in this case stated the two trap release times first and then the two trap densites. So for the table show below, the champion param column shows the following:\n",
    "    \n",
    "row 1 - release timescae for trap 1\n",
    "\n",
    "row 2 - release timescale for trap 2\n",
    "\n",
    "row 3 - trap density for trap 1\n",
    "\n",
    "row 4 - trap density for trap 2 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5c3a536-8313-411b-a90a-ea43c9990ee2",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimal_parameters(result[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9388855-ba94-4cbe-ba2c-980884bc6aa5",
   "metadata": {},
   "source": [
    "#### Champion heatmap "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bdec3c64-7cb8-42b0-98f0-c09c1c903b6b",
   "metadata": {},
   "source": [
    "The champion heatmap shows all of the results saved (the number of best decisions also dictates the number of individuals saved as part of the final xarray result). The plot shows the trap parameters against fitness. The trap parameters can be altered such that only trap release times or densities can be shown for example. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e2d7e10-f020-4372-8659-730af0bbce39",
   "metadata": {},
   "outputs": [],
   "source": [
    "champion_heatmap(result[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c231b60b-03c6-45c1-9a8d-70917678a36f",
   "metadata": {},
   "source": [
    "#### Simulated data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b6aa79a-6991-4c50-b9ba-60a371838149",
   "metadata": {},
   "source": [
    "Simulated data shows simulated, target profiles and plots them. The residual values can also be viewed and the best result from each island and singal level can be selected to be plotted. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d6b1d1ab-228c-48d4-8da6-b0f76cadf4d5",
   "metadata": {},
   "outputs": [],
   "source": [
    "display_simulated(result[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0216eeab-b659-4366-b45f-d002503d3532",
   "metadata": {},
   "outputs": [],
   "source": [
    "client.shutdown()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4a5aaad-39d5-47c0-998e-b6bf7a18e0de",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  },
  "vscode": {
   "interpreter": {
    "hash": "31f2aee4e71d21fbe5cf8b01ff0e069b9275f58929596ceb00d14d90e3e16cd6"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

# Tox (https://testrun.org/tox) is a tool for running tests
# Install:
#   pip install tox

# Get all available environments
#   tox -l
#   tox -av

# Run environment(s)
#   tox -e dev
#   tox -e mypy build

# Run all environments in parallel
#   tox -p auto

# Create a conda dev environment in '.tox/dev'
#   tox -e dev
#   conda activate .tox/dev
#  or
#   .tox/dev/bin/python

[tox]
minversion = 3.8
envlist = examples-{exposure, calibration,model_amplifier,model_inter_pixel}-{cli,notebook},
          examples-observation_{product,sequential,custom}-{cli,notebook},
          tutorials-{0,1,2,4,5,6,7},
          use_cases-{flex}-{cli,notebook}
          use_cases-APD-{cli,notebook}
          use_cases-CCD-{cli,notebook}
          use_cases-CMOS-{cli,notebook}
          use_cases-HxRG-{cli,notebook}

[testenv:build_latest_dev]
description = Build Book with latest Pyxel version
skip_install = True
deps =
    ipympl
    jupyter-book>=0.15
    pydata-sphinx-theme==0.14.3
    pyxel-sim[model,io]@git+https://gitlab.com/esa/pyxel.git
commands =
    python -c "import pyxel; pyxel.show_versions()"
    jupyter-book --version

    jupyter-book build --verbose .

[testenv:examples-exposure-{cli,notebook}]
description = Run an example
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/examples/exposure/. .

    # Run the pipeline
    cli: python -m pyxel run -vv exposure.yaml
    notebook: papermill exposure.ipynb output.ipynb
    cli: python -m pyxel run -vv exposure-source.yaml
    notebook: papermill exposure_persistence-H4RG.ipynb output.ipynb

[testenv:examples-observation_{product,sequential,custom}-{cli,notebook}]
description = Run an example
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/examples/observation/. .

    # Run the pipeline
    product-cli: python -m pyxel run -vv product.yaml
    product-notebook: papermill product.ipynb output.ipynb
    sequential-cli: python -m pyxel run -vv sequential.yaml
    sequential-notebook: papermill sequential.ipynb output.ipynb
    custom-cli: python -m pyxel run -vv custom.yaml
    custom-notebook: papermill custom.ipynb output.ipynb

[testenv:examples-calibration-{cli,notebook}]
description = Run an example
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/examples/calibration/. .

    # Run the pipeline
    cli: python -m pyxel run -vv calibration.yaml
    notebook: papermill calibration.ipynb output.ipynb


[testenv:examples-model_amplifier-{cli,notebook}]
description = Run an example
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r "{toxinidir}/examples/models/amplifier crosstalk/." .

    # Run the pipeline
    cli: python -m pyxel run -vv crosstalk.yaml
    notebook: papermill crosstalk.ipynb output.ipynb

[testenv:examples-model_inter_pixel-{cli,notebook}]
description = Run an example
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r "{toxinidir}/examples/models/inter-pixel capacitance/." .

    # Run the pipeline
    cli: python -m pyxel run -vv ipc.yaml
    notebook: papermill ipc.ipynb output.ipynb


[testenv:tutorials-{0,1,2,4,5,6,7}]
description = Run a tutorial
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/tutorial/. .

    # Run the pipeline
    0: papermill 00_introduction.ipynb output.ipynb
    1: papermill 01_first_simulation.ipynb output.ipynb
    2: papermill 02_pyxel_configuration_and_classes.ipynb output.ipynb
    4: papermill 04_observation_mode.ipynb output.ipynb
    5: papermill 05_calibration_mode.ipynb output.ipynb
    6: papermill 06_calibration_visualization.ipynb output.ipynb
    7: papermill 07_exposure_with_multiple_readouts.ipynb output.ipynb

[testenv:use_cases-{flex}-{cli,notebook}]
description = Run a use case
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}
    
    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    flex: cp -r {toxinidir}/use_cases/flex/. .

    # Run the pipeline
    cli: python -m pyxel run -vv pipeline.yaml
    notebook: papermill pipeline.ipynb output.ipynb

[testenv:use_cases-APD-{cli,notebook}]
description = Run a use case
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}

    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/use_cases/APD/. .

    # Run the pipeline
    cli: python -m pyxel run -vv saphira.yaml
    notebook: papermill saphira.ipynb output.ipynb

[testenv:use_cases-CCD-{cli,notebook}]
description = Run a use case
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}

    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/use_cases/CCD/. .

    # Run the pipeline
    cli: python -m pyxel run -vv ccd.yaml
    notebook: papermill ccd.ipynb output.ipynb

[testenv:use_cases-CMOS-{cli,notebook}]
description = Run a use case
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}

    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/use_cases/CMOS/. .

    # Run the pipeline
    cli: python -m pyxel run -vv cmos.yaml
    notebook: papermill cmos.ipynb output.ipynb

[testenv:use_cases-HxRG-{cli,notebook}]
description = Run a use case
skip_install = True
changedir = {envdir}/work
sitepackages = True
allowlist_externals =
    cp
deps =
    notebook: papermill
commands =
    {posargs}

    # Check if Pyxel is installed
    python -c "import pyxel;pyxel.show_versions()"

    # Copy data
    cp -r {toxinidir}/use_cases/HxRG/. .

    # Run the pipeline
    cli: python -m pyxel run -vv h2rg.yaml
    notebook: papermill h2rg.ipynb output.ipynb
